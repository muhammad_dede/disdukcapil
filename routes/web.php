<?php

use App\Http\Controllers\PagesController;
use App\Http\Controllers\User\AccountController;
use App\Http\Controllers\User\ChatController;
use App\Http\Controllers\User\NotifikasiController;
use App\Http\Controllers\User\PengajuanController;
use Illuminate\Support\Facades\Route;

Route::get('/', [PagesController::class, 'index'])->name('index');
Route::get('/layanan', [PagesController::class, 'showLayanan'])->name('layanan');
Route::get('/layanan/{url}', [PagesController::class, 'detailLayanan'])->name('layanan.detail');
Route::get('/tanya-jawab', [PagesController::class, 'showTanyaJawab'])->name('tanya-jawab');
Route::get('/kontak-kami', [PagesController::class, 'showKontakKami'])->name('kontak-kami');
Route::post('/send-message', [PagesController::class, 'sendMessage'])->name('send-message');

Route::group(['middleware' => 'auth', 'prefix' => 'user', 'as' => 'user.'], function () {
    Route::group(['prefix' => 'pengajuan', 'as' => 'pengajuan.'], function () {
        Route::get('/', [PengajuanController::class, 'index'])->name('index');
        Route::get('/{url}/create', [PengajuanController::class, 'create'])->name('create');
        Route::post('/{url}/store', [PengajuanController::class, 'store'])->name('store');
        Route::get('/{no_pengajuan}/show', [PengajuanController::class, 'show'])->name('show');
        Route::post('/{no_pengajuan}/cancel', [PengajuanController::class, 'cancel'])->name('cancel');
        Route::get('/dokumen/{file}/download', [PengajuanController::class, 'downloadDokumen'])->name('download-dokumen');
    });
    Route::group(['prefix' => 'notifikasi', 'as' => 'notifikasi.'], function () {
        Route::get('/', [NotifikasiController::class, 'index'])->name('index');
        Route::get('/data', [NotifikasiController::class, 'data'])->name('data');
        Route::get('/count', [NotifikasiController::class, 'count'])->name('count');
        Route::get('/read/all', [NotifikasiController::class, 'readAll'])->name('read-all');
        Route::get('/{id}/read', [NotifikasiController::class, 'read'])->name('read');
    });
    Route::group(['prefix' => 'account', 'as' => 'account.'], function () {
        Route::get('/', [AccountController::class, 'index'])->name('index');
        Route::post('/update', [AccountController::class, 'update'])->name('update');
        Route::get('/change-password', [AccountController::class, 'changePassword'])->name('change-password');
        Route::post('/update-password', [AccountController::class, 'updatePassword'])->name('update-password');
    });

    Route::group(['prefix' => 'chat', 'as' => 'chat.'], function () {
        Route::get('/{no_pengajuan}', [ChatController::class, 'show'])->name('show');
        Route::post('/{no_pengajuan}', [ChatController::class, 'send'])->name('send');
    });
});

Auth::routes();
