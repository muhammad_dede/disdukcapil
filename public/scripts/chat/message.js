(function (window, undefined) {
    'use strict';

    let no_pengajuan = $(".no_pengajuan").attr('data');

    let pusher = new Pusher('78256e98206ec9b69fb1', {
        cluster: 'ap1'
    });

    let channel = pusher.subscribe('chat-user-channel');
    channel.bind('chat-user-event', function (data) {
        if (no_pengajuan == data.message.no_pengajuan) {
            $(".chat_message").append(`
                <div class="media w-50 mb-3">
                    <img src="`+ window.location.origin + `/favicon.png" alt="operator" width="50"
                        class="rounded-circle">
                    <div class="media-body ml-3">
                        <div class="bg-light rounded py-2 px-3 mb-1">
                            <p class="text-small mb-0 text-muted">
                                `+ data.message.body + `
                            </p>
                        </div>
                        <p class="small text-muted">
                            `+ data.message.waktu + ` |
                            `+ data.message.tanggal + `
                        </p>
                    </div>
                </div>
            `);
            scrollToBottom();
        }
    });

    scrollToBottom();

    function scrollToBottom() {
        $(".chat_message").scrollTop($(".chat_message")[0].scrollHeight);
    }

})(window);
