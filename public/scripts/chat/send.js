(function (window, undefined) {
    'use strict';

    $('.form_chat').submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: $(this).attr("action"),
            method: "POST",
            data: $(this).serialize(),
            beforeSend: function () {
                $(document).find('.text_error').text('');
            },
            success: function (response) {
                if (response.failed) {
                    $.each(response.failed, function (field, val) {
                        $('#' + field + '_error').text(val[0]);
                    });
                } else if (response.data) {
                    $(".chat_message").append(`<div class="media w-50 ml-auto mb-3">
                                <div class="media-body">
                                    <div class="bg-primary rounded py-2 px-3 mb-2">
                                        <p class="text-small mb-0 text-white">` + response.data.body + `</p>
                                    </div>
                                    <p class="small text-muted">` + response.data.waktu + ` | ` + response.data
                            .tanggal + `</p>
                                </div>
                            </div>`);
                    $("#message").val("");
                    $(".chat_message").scrollTop($(".chat_message")[0].scrollHeight);
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    });

})(window);
