$(document).ready(function () {

    var channel = pusher.subscribe('notif-channel');
    channel.bind('notif-event', function (data) {
        data_notifikasi();
    });

    data_notifikasi();

    function data_notifikasi() {
        $.ajax({
            url: window.location.origin + '/user/notifikasi/data',
            method: 'get',
            dataType: 'json',
            success: function (response) {
                $.each(response.data_notifikasi, function (key, item) {
                    var created_at = new Date(item.created_at);
                    var tanggal = created_at.getDate() + '-' + created_at.getMonth() + '-' + created_at.getFullYear();
                    var jam = created_at.getHours();
                    var min = created_at.getMinutes();
                    if (min < 10)
                        minute = "0" + min;
                    else
                        minute = min;
                    $("tbody").append('<tr class="' + (item.readed == false ? 'table-warning' : '') + '">\
                                            <td>\
                                                <a href="'+ window.location.origin + '/user/notifikasi/' + item.id + '/read' + '" >\
                                                    <h5>'+ item.judul + '</h5>\
                                                    <p>'+ item.deskripsi + '</p>\
                                                    <small>' + tanggal + ', ' + jam + ':' + minute + '</small>\
                                                </a>\
                                            </td>\
                                    </tr>');
                });
            },
        });
    }
});
