$(document).ready(function () {

    var channel = pusher.subscribe('notif-channel');
    channel.bind('notif-event', function (data) {
        count_notifikasi();
    });

    count_notifikasi();

    function count_notifikasi() {
        $.ajax({
            url: window.location.origin + '/user/notifikasi/count',
            method: 'get',
            dataType: 'json',
            success: function (response) {
                if (response.count_notifikasi > 0) {
                    $(".count_notifikasi").addClass('badge badge-danger');
                    $(".count_notifikasi").html(response.count_notifikasi);
                } else {
                    $(".count_notifikasi").removeClass('badge badge-danger');
                    $(".count_notifikasi").html('');

                }
            },
        });
    }
});
