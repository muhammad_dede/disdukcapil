<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Rules\CheckCurrentPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function index()
    {
        return view('account.index');
    }

    public function update(Request $request)
    {
        $request->validate([
            'nik' => 'required|numeric|digits_between:15,20|unique:warga,nik,' . auth()->id() . ',id',
            'nama_lengkap' => 'required|string|max:255',
            'tempat_lahir' => 'required|string|max:255',
            'tgl_lahir' => 'required|date_format:Y-m-d',
            'jenis_kelamin' => 'required|string|max:255',
            'nama_ibu' => 'required|string|max:255',
            'gol_darah' => 'required|string|max:255',
            'kewarganegaraan' => 'required|string|max:255',
            'alamat' => 'required|string|max:255',
            'rt' => 'required|string|max:5',
            'rw' => 'required|string|max:5',
            'kelurahan' => 'required|string|max:255',
            'kecamatan' => 'required|string|max:255',
            'kota' => 'required|string|max:255',
            'provinsi' => 'required|string|max:255',
            'kode_pos' => 'nullable|string|max:10',
            'no_whatsapp' => 'nullable|string|max:30',
            'email' => 'required|string|email|unique:warga,email,' . auth()->id() . ',id',
            'profil' => 'nullable|mimes:png,jpeg,jpg|max:1024',
        ]);

        if ($request->file('profil')) {
            if (auth()->user()->profil !== 'default.svg') {
                File::delete('profil-user/' . auth()->user()->profil);
            }

            $profil = 'profil-' . auth()->id() . '.' . $request->profil->extension();
            $request->profil->move(public_path('profil-user'), $profil);
        } else {
            $profil = auth()->user()->profil;
        }

        User::where('id', auth()->id())->update([
            'nama_lengkap' => $request->nama_lengkap,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'nama_ibu' => $request->nama_ibu,
            'gol_darah' => $request->gol_darah,
            'kewarganegaraan' => $request->kewarganegaraan,
            'alamat' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'kelurahan' => $request->kelurahan,
            'kecamatan' => $request->kecamatan,
            'kota' => $request->kota,
            'provinsi' => $request->provinsi,
            'kode_pos' => $request->kode_pos,
            'no_whatsapp' => $request->no_whatsapp,
            'email' => strtolower($request->email),
            'profil' => $profil,
        ]);

        return redirect()->back()->with('success', 'BERHASIL MEMPERBAHARUI AKUN ANDA.');
    }

    public function changePassword()
    {
        return view('account.change-password');
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required', 'string', new CheckCurrentPassword],
            'password' => 'required|confirmed|string|min:8',
        ]);

        User::where('id', auth()->id())->update([
            'password' => Hash::make($request->password),
        ]);

        return redirect()->back()->with('success', 'BERHASIL MENGUBAH PASSWORD ANDA.');
    }
}
