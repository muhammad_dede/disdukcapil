<?php

namespace App\Http\Controllers\User;

use App\Events\ChatWarga;
use App\Http\Controllers\Controller;
use App\Models\Pengajuan;
use App\Models\PengajuanChat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChatController extends Controller
{
    public function show($no_pengajuan)
    {
        $pengajuan = Pengajuan::where('no_pengajuan', $no_pengajuan)->with(['pengajuanChat'])->first();
        if (!$pengajuan) {
            return abort('404');
        }
        return view('chat.show', compact('pengajuan'));
    }

    public function send(Request $request, $no_pengajuan)
    {
        if ($request->ajax()) {
            $validator = Validator::make($request->all(), [
                'message' => 'required|string|max:255',
            ]);

            if ($validator->fails()) {
                return response()->json(['failed' => $validator->errors()->toArray()]);
            }

            PengajuanChat::where('no_pengajuan', $no_pengajuan)->where('to_warga', true)->update([
                'readed' => true,
            ]);

            $pengajuan_chat = PengajuanChat::create([
                'no_pengajuan' => $no_pengajuan,
                'body' => $request->message,
                'to_warga' => false,
                'to_user' => true,
                'readed' => false,
            ]);

            $data = [
                'no_pengajuan' => $pengajuan_chat->no_pengajuan,
                'profil' => $pengajuan_chat->pengajuan->warga->profil,
                'body' => $pengajuan_chat->body,
                'waktu' => date('h:i A', strtotime($pengajuan_chat->created_at)),
                'tanggal' => date('d M Y', strtotime($pengajuan_chat->created_at))
            ];

            event(new ChatWarga($data));

            return response()->json(['data' => $data]);
        }
    }
}
