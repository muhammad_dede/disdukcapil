<?php

namespace App\Http\Controllers\User;

use App\Events\SendNotification;
use App\Http\Controllers\Controller;
use App\Models\DataKelahiran;
use App\Models\DataLahirMati;
use App\Models\DataPembatalanPerkawinan;
use App\Models\DataPengajuan\KtpElektronikBaru;
use App\Models\DataPengajuan\KtpElektronikPerubahan;
use App\Models\DataPerkawinan;
use App\Models\Layanan;
use App\Models\LayananDokumen;
use App\Models\Notifikasi;
use App\Models\Pengajuan;
use App\Models\PengajuanRiwayat;
use App\Models\PengajuanUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class PengajuanController extends Controller
{
    public function index()
    {
        return view('pengajuan.index');
    }

    public function create($url)
    {
        $layanan = Layanan::with(['layananDokumen', 'layananIsian', 'layananPersyaratan'])->where('url', $url)->first();
        if (!$layanan) {
            return abort('404');
        }
        return view('pengajuan.create', compact('layanan'));
    }

    public function store(Request $request, $url)
    {
        $layanan = Layanan::with(['layananDokumen', 'layananIsian', 'layananPersyaratan'])->where('url', $url)->first();
        if (!$layanan) {
            return abort('404');
        }

        // CEK JIKA SUDAH MENGAJUKAN
        $cek_sudah_mengajukan = Pengajuan::where('id_warga', auth()->id())->where('id_layanan', $layanan->id)->where('status', '!=', '3');
        if ($cek_sudah_mengajukan->count() > 0) {
            return redirect()->back()->with('failed', 'ANDA SUDAH MENGAJUKAN LAYANAN INI, HARAP CEK DAFTAR PENGAJUAN ANDA!');
        }

        // validasi berkas persyaratan
        if ($request->berkas) {
            foreach ($request->berkas as $index => $berkas) {
                if ($berkas['wajib'] == 1) {
                    $request->validate([
                        'berkas.' . $index . '.file' => 'required|mimes:jpeg,jpg,png,pdf|max:1200',
                    ]);
                } else {
                    $request->validate([
                        'berkas.' . $index . '.file' => 'nullable|mimes:jpeg,jpg,png,pdf|max:1200',
                    ]);
                }
            }
        }

        if ($layanan->id == 1) {
            $request->validate([
                'nama_lengkap' => 'required|string|max:255',
                'tempat_lahir' => 'required|string|max:255',
                'tgl_lahir' => ['required', 'date_format:Y-m-d'],
                'jenis_kelamin' => 'required|string|max:255',
                'nama_ibu' => 'required|string|max:255',
                'gol_darah' => 'required|string|max:255',
                'kewarganegaraan' => 'required|string|max:255',
                'alamat' => 'required|string|max:255',
                'rt' => 'required|string|max:5',
                'rw' => 'required|string|max:5',
                'kelurahan' => 'required|string|max:255',
                'kecamatan' => 'required|string|max:255',
                'kota' => 'required|string|max:255',
                'provinsi' => 'required|string|max:255',
                'kode_pos' => 'nullable|string|max:10',
                'terms' => 'required',
            ]);

            $data_pengajuan = KtpElektronikBaru::create([
                'nama_lengkap' => $request->nama_lengkap,
                'tempat_lahir' => $request->tempat_lahir,
                'tgl_lahir' => $request->tgl_lahir,
                'jenis_kelamin' => $request->jenis_kelamin,
                'nama_ibu' => $request->nama_ibu,
                'gol_darah' => $request->gol_darah,
                'kewarganegaraan' => $request->kewarganegaraan,
                'alamat' => $request->alamat,
                'rt' => $request->rt,
                'rw' => $request->rw,
                'kelurahan' => $request->kelurahan,
                'kecamatan' => $request->kecamatan,
                'kota' => $request->kota,
                'provinsi' => $request->provinsi,
                'kode_pos' => $request->kode_pos,
            ]);
        } elseif ($layanan->id == 2) {
            $request->validate([
                'nama_lengkap' => 'required|string|max:255',
                'nik' => 'required|numeric|digits:16,16',
                'alamat_rumah' => 'required|string|max:255',
                'pendidikan_terakhir_semula' => 'required|string|max:255',
                'pendidikan_terakhir_menjadi' => 'required|string|max:255',
                'pendidikan_terakhir_dasar_perubahan' => 'nullable|string|max:255',
                'pendidikan_terakhir_no' => 'nullable|string|max:255',
                'pendidikan_terakhir_tgl' => 'nullable|date_format:Y-m-d',
                'pekerjaan_semula' => 'required|string|max:255',
                'pekerjaan_menjadi' => 'required|string|max:255',
                'pekerjaan_dasar_perubahan' => 'nullable|string|max:255',
                'pekerjaan_no' => 'nullable|string|max:255',
                'pekerjaan_tgl' => 'nullable|date_format:Y-m-d',
                'agama_semula' => 'required|string|max:255',
                'agama_menjadi' => 'required|string|max:255',
                'agama_dasar_perubahan' => 'nullable|string|max:255',
                'agama_no' => 'nullable|string|max:255',
                'agama_tgl' => 'nullable|date_format:Y-m-d',
                'perubahan_lainnya_semula' => 'nullable|string|max:255',
                'perubahan_lainnya_menjadi' => 'nullable|string|max:255',
                'perubahan_lainnya_dasar_perubahan' => 'nullable|string|max:255',
                'perubahan_lainnya_no' => 'nullable|string|max:255',
                'perubahan_lainnya_tgl' => 'nullable|date_format:Y-m-d',
            ]);

            $data_pengajuan = KtpElektronikPerubahan::create([
                'nama_lengkap' => $request->nama_lengkap,
                'nik' => $request->nik,
                'alamat_rumah' => $request->alamat_rumah,
                'pendidikan_terakhir_semula' => $request->pendidikan_terakhir_semula,
                'pendidikan_terakhir_menjadi' => $request->pendidikan_terakhir_menjadi,
                'pendidikan_terakhir_dasar_perubahan' => $request->pendidikan_terakhir_dasar_perubahan,
                'pendidikan_terakhir_no' => $request->pendidikan_terakhir_no,
                'pendidikan_terakhir_tgl' => $request->pendidikan_terakhir_tgl,
                'pekerjaan_semula' => $request->pekerjaan_semula,
                'pekerjaan_menjadi' => $request->pekerjaan_menjadi,
                'pekerjaan_dasar_perubahan' => $request->pekerjaan_dasar_perubahan,
                'pekerjaan_no' => $request->pekerjaan_no,
                'pekerjaan_tgl' => $request->pekerjaan_tgl,
                'agama_semula' => $request->agama_semula,
                'agama_menjadi' => $request->agama_menjadi,
                'agama_dasar_perubahan' => $request->agama_dasar_perubahan,
                'agama_no' => $request->agama_no,
                'agama_tgl' => $request->agama_tgl,
                'perubahan_lainnya_semula' => $request->perubahan_lainnya_semula,
                'perubahan_lainnya_menjadi' => $request->perubahan_lainnya_menjadi,
                'perubahan_lainnya_dasar_perubahan' => $request->perubahan_lainnya_dasar_perubahan,
                'perubahan_lainnya_no' => $request->perubahan_lainnya_no,
                'perubahan_lainnya_tgl' => $request->perubahan_lainnya_tgl,
            ]);
        } elseif ($layanan->id == 3) {
            $request->validate([
                'nama_pelapor' => 'required|string|max:255',
                'nik' => 'required|numeric|digits:16,16',
                'nomor_kartu_keluarga' => 'required|string|max:255',
                'kewarganegaraan' => 'required|string|max:255',
                'nama_anak' => 'required|string|max:255',
                'jenis_kelamin_anak' => 'required|string|max:255',
                'tmpt_dilahirkan_anak' => 'required|string|max:255',
                'tmpt_kelahiran_anak' => 'required|string|max:255',
                'hari_lahir_anak' => 'required|string|max:255',
                'tgl_lahir_anak' => ['required', 'date_format:Y-m-d'],
                'waktu_lahir_anak' => 'required|date_format:H:i',
                'jenis_kelahiran_anak' => 'required|string|max:255',
                'kelahiran_ke_anak' => 'required|string|max:255',
                'penolong_kelahiran_anak' => 'required|string|max:255',
                'berat_bayi_anak' => 'required|string|max:255',
                'panjang_bayi_anak' => 'required|string|max:255',
                'nama_ayah' => 'required|string|max:255',
                'nik_ayah' => 'required|numeric|digits:16,16',
                'tempat_lahir_ayah' => 'required|string|max:255',
                'tgl_lahir_ayah' => ['required', 'date_format:Y-m-d'],
                'kewarganegaraan_ayah' => 'required|string|max:255',
                'nama_ibu' => 'required|string|max:255',
                'nik_ibu' => 'required|numeric|digits:16,16',
                'tempat_lahir_ibu' => 'required|string|max:255',
                'tgl_lahir_ibu' => ['required', 'date_format:Y-m-d'],
                'kewarganegaraan_ibu' => 'required|string|max:255',
                'nama_saksi_1' => 'required|string|max:255',
                'nik_saksi_1' => 'required|numeric|digits:16,16',
                'no_kk_saksi_1' => 'required|string|max:255',
                'kewarganegaraan_saksi_1' => 'required|string|max:255',
                'nama_saksi_2' => 'required|string|max:255',
                'nik_saksi_2' => 'required|numeric|digits:16,16',
                'no_kk_saksi_2' => 'required|string|max:255',
                'kewarganegaraan_saksi_2' => 'required|string|max:255',
            ]);

            $data_pengajuan = DataKelahiran::create([
                'nama_pelapor' => $request->nama_pelapor,
                'nik' => $request->nik,
                'nomor_kartu_keluarga' => $request->nomor_kartu_keluarga,
                'kewarganegaraan' => $request->kewarganegaraan,
                'nama_anak' => $request->nama_anak,
                'jenis_kelamin_anak' => $request->jenis_kelamin_anak,
                'tmpt_dilahirkan_anak' => $request->tmpt_dilahirkan_anak,
                'tmpt_kelahiran_anak' => $request->tmpt_kelahiran_anak,
                'hari_lahir_anak' => $request->hari_lahir_anak,
                'tgl_lahir_anak' => $request->tgl_lahir_anak,
                'waktu_lahir_anak' => $request->waktu_lahir_anak,
                'jenis_kelahiran_anak' => $request->jenis_kelahiran_anak,
                'kelahiran_ke_anak' =>  $request->kelahiran_ke_anak,
                'penolong_kelahiran_anak' => $request->penolong_kelahiran_anak,
                'berat_bayi_anak' => $request->berat_bayi_anak,
                'panjang_bayi_anak' => $request->panjang_bayi_anak,
                'nama_ayah' => $request->nama_ayah,
                'nik_ayah' => $request->nik_ayah,
                'tempat_lahir_ayah' => $request->tempat_lahir_ayah,
                'tgl_lahir_ayah' => $request->tgl_lahir_ayah,
                'kewarganegaraan_ayah' => $request->kewarganegaraan_ayah,
                'nama_ibu' => $request->nama_ibu,
                'nik_ibu' => $request->nik_ibu,
                'tempat_lahir_ibu' => $request->tempat_lahir_ibu,
                'tgl_lahir_ibu' => $request->tgl_lahir_ibu,
                'tgl_lahir_ayah' => $request->tgl_lahir_ayah,
                'kewarganegaraan_ibu' => $request->kewarganegaraan_ibu,
                'nama_saksi_1' => $request->nama_saksi_1,
                'nik_saksi_1' => $request->nik_saksi_1,
                'no_kk_saksi_1' => $request->no_kk_saksi_1,
                'kewarganegaraan_saksi_1' => $request->kewarganegaraan_saksi_1,
                'nama_saksi_2' => $request->nama_saksi_2,
                'nik_saksi_2' => $request->nik_saksi_2,
                'no_kk_saksi_2' => $request->no_kk_saksi_2,
                'kewarganegaraan_saksi_2' => $request->kewarganegaraan_saksi_2,
            ]);
        } elseif ($layanan->id == 4) {
            $request->validate([
                'nama_pelapor' => 'required|string|max:255',
                'nik' => 'required|numeric|digits:16,16',
                'nomor_kartu_keluarga' => 'required|string|max:255',
                'kewarganegaraan' => 'required|string|max:255',
                'lamanya_dalam_kandungan' => 'required|string|max:3',
                'jenis_kelamin' => 'required|string|max:255',
                'tgl_lahir_mati' => ['required', 'date_format:Y-m-d'],
                'jenis_kelahiran' => 'required|string|max:255',
                'anak_ke' => 'required|numeric|max:3',
                'tempat_dilahirkan' => 'required|string|max:255',
                'penolong_kelahiran' => 'required|string|max:255',
                'sebab_lahir_mati' => 'required|string|max:255',
                'yang_menentukan' => 'required|string|max:255',
                'tempat_kelahiran' => 'required|string|max:255',
                'nama_ayah' => 'required|string|max:255',
                'nik_ayah' => 'required|numeric|digits:16,16',
                'tempat_lahir_ayah' => 'required|string|max:255',
                'tgl_lahir_ayah' => ['required', 'date_format:Y-m-d'],
                'kewarganegaraan_ayah' => 'required|string|max:255',
                'nama_ibu' => 'required|string|max:255',
                'nik_ibu' => 'required|numeric|digits:16,16',
                'tempat_lahir_ibu' => 'required|string|max:255',
                'tgl_lahir_ibu' => ['required', 'date_format:Y-m-d'],
                'kewarganegaraan_ibu' => 'required|string|max:255',
                'nama_saksi_1' => 'required|string|max:255',
                'nik_saksi_1' => 'required|numeric|digits:16,16',
                'no_kk_saksi_1' => 'required|string|max:255',
                'kewarganegaraan_saksi_1' => 'required|string|max:255',
                'nama_saksi_2' => 'required|string|max:255',
                'nik_saksi_2' => 'required|numeric|digits:16,16',
                'no_kk_saksi_2' => 'required|string|max:255',
                'kewarganegaraan_saksi_2' => 'required|string|max:255',
            ]);

            $data_pengajuan = DataLahirMati::create([
                'nama_pelapor' => $request->nama_pelapor,
                'nik' => $request->nik,
                'nomor_kartu_keluarga' => $request->nomor_kartu_keluarga,
                'kewarganegaraan' => $request->kewarganegaraan,
                'lamanya_dalam_kandungan' => $request->lamanya_dalam_kandungan,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tgl_lahir_mati' => $request->tgl_lahir_mati,
                'jenis_kelahiran' => $request->jenis_kelahiran,
                'anak_ke' => $request->anak_ke,
                'tempat_dilahirkan' => $request->tempat_dilahirkan,
                'penolong_kelahiran' => $request->penolong_kelahiran,
                'sebab_lahir_mati' => $request->sebab_lahir_mati,
                'yang_menentukan' =>  $request->yang_menentukan,
                'tempat_kelahiran' => $request->tempat_kelahiran,
                'nama_ayah' => $request->nama_ayah,
                'nik_ayah' => $request->nik_ayah,
                'tempat_lahir_ayah' => $request->tempat_lahir_ayah,
                'tgl_lahir_ayah' => $request->tgl_lahir_ayah,
                'kewarganegaraan_ayah' => $request->kewarganegaraan_ayah,
                'nama_ibu' => $request->nama_ibu,
                'nik_ibu' => $request->nik_ibu,
                'tempat_lahir_ibu' => $request->tempat_lahir_ibu,
                'tgl_lahir_ibu' => $request->tgl_lahir_ibu,
                'tgl_lahir_ayah' => $request->tgl_lahir_ayah,
                'kewarganegaraan_ibu' => $request->kewarganegaraan_ibu,
                'nama_saksi_1' => $request->nama_saksi_1,
                'nik_saksi_1' => $request->nik_saksi_1,
                'no_kk_saksi_1' => $request->no_kk_saksi_1,
                'kewarganegaraan_saksi_1' => $request->kewarganegaraan_saksi_1,
                'nama_saksi_2' => $request->nama_saksi_2,
                'nik_saksi_2' => $request->nik_saksi_2,
                'no_kk_saksi_2' => $request->no_kk_saksi_2,
                'kewarganegaraan_saksi_2' => $request->kewarganegaraan_saksi_2,
            ]);
        } elseif ($layanan->id == 5) {
            $request->validate([
                'nama_pelapor' => 'required|string|max:255',
                'nik' => 'required|string|max:255',
                'nomor_kartu_keluarga' => 'required|string|max:255',
                'kewarganegaraan' => 'required|string|max:255',
                'nik_ayah_dari_suami' => 'required|numeric|digits:16,16',
                'nama_ayah_dari_suami' => 'required|string|max:255',
                'nik_ibu_dari_suami' => 'required|numeric|digits:16,16',
                'nama_ibu_dari_suami' => 'required|string|max:255',
                'nik_ayah_dari_istri' => 'required|numeric|digits:16,16',
                'nama_ayah_dari_istri' => 'required|string|max:255',
                'nik_ibu_dari_istri' => 'required|numeric|digits:16,16',
                'nama_ibu_dari_istri' => 'required|string|max:255',
                'status_perkawinan_sebelum_kawin' => 'required|string|max:255',
                'perkawinan_yang_ke' => 'required|string|max:255',
                'istri_yang_ke' => 'required|numeric|max:3',
                'tgl_pemberkatan_perkawinan' => ['required', 'date_format:Y-m-d'],
                'tgl_melapor' => ['required', 'date_format:Y-m-d'],
                'jam_pelaporan' => 'required|date_format:H:i',
                'agama_kepercayaan' => 'required|string|max:255',
                'nama_pengadilan' => 'required|string|max:255',
                'no_penetapan_pengadilan' => 'required|string|max:255',
                'tgl_penepatan_pengadilan' => ['required', 'date_format:Y-m-d'],
                'nama_pemuka_agama' => 'required|string|max:255',
                'no_surat_izin_dari_perwakilan' => 'required|string|max:255',
                'perjanjian_perkawinan' => 'required|string|max:255',
                'no_akta_notaris' => 'required|string|max:255',
                'tgl_akta_notaris' => ['required', 'date_format:Y-m-d'],
            ]);

            $data_pengajuan = DataPerkawinan::create([
                'nama_pelapor' => $request->nama_pelapor,
                'nik' => $request->nik,
                'nomor_kartu_keluarga' => $request->nomor_kartu_keluarga,
                'kewarganegaraan' => $request->kewarganegaraan,
                'nik_ayah_dari_suami' => $request->nik_ayah_dari_suami,
                'nama_ayah_dari_suami' => $request->nama_ayah_dari_suami,
                'nik_ibu_dari_suami' => $request->nik_ibu_dari_suami,
                'nama_ibu_dari_suami' => $request->nama_ibu_dari_suami,
                'nik_ayah_dari_istri' => $request->nik_ayah_dari_istri,
                'nama_ayah_dari_istri' => $request->nama_ayah_dari_istri,
                'nik_ibu_dari_istri' => $request->nik_ibu_dari_istri,
                'nama_ibu_dari_istri' => $request->nama_ibu_dari_istri,
                'status_perkawinan_sebelum_kawin' => $request->status_perkawinan_sebelum_kawin,
                'perkawinan_yang_ke' =>  $request->perkawinan_yang_ke,
                'istri_yang_ke' => $request->istri_yang_ke,
                'tgl_pemberkatan_perkawinan' => $request->tgl_pemberkatan_perkawinan,
                'tgl_melapor' => $request->tgl_melapor,
                'jam_pelaporan' => $request->jam_pelaporan,
                'agama_kepercayaan' => $request->agama_kepercayaan,
                'nama_organisasi_kepercayaan' => $request->nama_organisasi_kepercayaan,
                'nama_pengadilan' => $request->nama_pengadilan,
                'no_penetapan_pengadilan' => $request->no_penetapan_pengadilan,
                'tgl_penepatan_pengadilan' => $request->tgl_penepatan_pengadilan,
                'nama_pemuka_agama' => $request->nama_pemuka_agama,
                'no_surat_izin_dari_perwakilan' => $request->no_surat_izin_dari_perwakilan,
                'no_passport' => $request->no_passport,
                'perjanjian_perkawinan' => $request->perjanjian_perkawinan,
                'no_akta_notaris' => $request->no_akta_notaris,
                'tgl_akta_notaris' => $request->tgl_akta_notaris,
                'jumlah_anak' => $request->jumlah_anak,
            ]);
        } elseif ($layanan->id == 6) {
            $request->validate([
                'nama_pelapor' => 'required|string|max:255',
                'nik' => 'required|string|max:255',
                'nomor_kartu_keluarga' => 'required|string|max:255',
                'kewarganegaraan' => 'required|string|max:255',
                'tgl_perkawinan' => ['required', 'date_format:Y-m-d'],
                'no_akta_perkawinan' => 'required|string|max:255',
                'tgl_akta_perkawinan' => ['required', 'date_format:Y-m-d'],
                'nama_pengadilan_pembatalan' => 'required|string|max:255',
                'no_putusan_pengadilan' => 'required|string|max:255',
                'tgl_putusan_pengadilan' => ['required', 'date_format:Y-m-d'],
                'tgl_pelaporan_perkawinan_luar_negeri' => ['required', 'date_format:Y-m-d'],
            ]);

            $data_pengajuan = DataPembatalanPerkawinan::create([
                'nama_pelapor' => $request->nama_pelapor,
                'nik' => $request->nik,
                'nomor_kartu_keluarga' => $request->nomor_kartu_keluarga,
                'kewarganegaraan' => $request->kewarganegaraan,
                'tgl_perkawinan' => $request->tgl_perkawinan,
                'no_akta_perkawinan' => $request->no_akta_perkawinan,
                'tgl_akta_perkawinan' => $request->tgl_akta_perkawinan,
                'nama_pengadilan_pembatalan' => $request->nama_pengadilan_pembatalan,
                'no_putusan_pengadilan' => $request->no_putusan_pengadilan,
                'tgl_putusan_pengadilan' => $request->tgl_putusan_pengadilan,
                'tgl_pelaporan_perkawinan_luar_negeri' => $request->tgl_pelaporan_perkawinan_luar_negeri,
            ]);
        }

        // Create Pengajuan
        $pengajuan = $data_pengajuan->pengajuan()->create([
            'no_pengajuan' => time(),
            'id_layanan' => $layanan->id,
            'id_warga' => auth()->id(),
            'status' => 1,
        ]);

        // create riwayat
        PengajuanRiwayat::create([
            'no_pengajuan' => $pengajuan->no_pengajuan,
            'keterangan' => $pengajuan->statusPengajuan->keterangan,
        ]);

        // Upload berkas persyaratan
        if ($request->berkas) {
            foreach ($request->berkas as $index => $berkas) {
                $file_berkas = $berkas['file'];
                $file = time() . $berkas['id_persyaratan'] . '.' . $file_berkas->extension();
                $file_berkas->move(public_path('upload-persyaratan'), $file);

                PengajuanUpload::create([
                    'no_pengajuan' => $pengajuan->no_pengajuan,
                    'id_persyaratan' => $berkas['id_persyaratan'],
                    'file' => $file,
                ]);
            }
        }

        $notifikasi = Notifikasi::create([
            'no_pengajuan' => $pengajuan->no_pengajuan,
            'kategori' => 'pengajuan',
            'judul' => $pengajuan->statusPengajuan->nama,
            'deskripsi' => $pengajuan->statusPengajuan->keterangan,
            'id_warga' => auth()->id(),
        ]);

        event(new SendNotification($notifikasi));

        return redirect()->route('user.pengajuan.index')->with('success', 'BERHASIL MENGAJUKAN, MOHON MENUNGGU KONFIRMASI DARI OPERATOR KAMI.');
    }

    public function show($no_pengajuan)
    {
        $pengajuan = Pengajuan::with(['pengajuanUpload', 'pengajuanRiwayat', 'data_pengajuan'])->where('no_pengajuan', $no_pengajuan)->where('id_warga', auth()->id())->first();
        if (!$pengajuan) {
            return abort('404');
        }

        return view('pengajuan.show', compact('pengajuan'));
    }

    public function cancel($no_pengajuan)
    {
        $pengajuan = Pengajuan::where('no_pengajuan', $no_pengajuan)->first();
        if (!$pengajuan) {
            return abort('404');
        }

        $pengajuan->update([
            'status' => 3,
        ]);

        PengajuanRiwayat::create([
            'no_pengajuan' => $no_pengajuan,
            'keterangan' => 'Pemohon membatalkan pengajuan layanan',
        ]);

        $notifikasi = Notifikasi::create([
            'no_pengajuan' => $pengajuan->no_pengajuan,
            'kategori' => 'pengajuan',
            'judul' => 'Pembatalan Pengajuan',
            'deskripsi' => 'Pemohon melakukan pembatalan pengajuan',
            'id_warga' => auth()->id(),
        ]);

        event(new SendNotification($notifikasi));

        return redirect()->back()->with('success', 'BERHASIL MEMBATALKAN PENGAJUAN, PEMBATALAN PENGAJUAN DAPAT DILIHAT PADA RIWAYAT PENGAJUAN ANDA.');
    }

    public function downloadDokumen($file)
    {
        $filepath = public_path('dokumen-pendukung/' . $file);
        return Response::download($filepath);
    }
}
