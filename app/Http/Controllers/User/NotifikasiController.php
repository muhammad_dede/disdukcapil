<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Notifikasi;
use App\Models\Pengajuan;
use Illuminate\Http\Request;

class NotifikasiController extends Controller
{
    public function index()
    {
        return view('notifikasi.index');
    }

    public function data()
    {
        $data_notifikasi = Notifikasi::where('id_warga', auth()->id())->orderBy('created_at', 'desc')->get();
        return response()->json(['data_notifikasi' => $data_notifikasi]);
    }

    public function count()
    {
        $count_notifikasi = Notifikasi::where('id_warga', auth()->id())->where('readed', false)->count();
        return response()->json(['count_notifikasi' => $count_notifikasi]);
    }

    public function readAll()
    {
        $notifikasi = Notifikasi::where('id_warga', auth()->id());
        $notifikasi->update([
            'readed' => true,
        ]);
        return redirect()->back();
    }

    public function read($id)
    {
        $notifikasi = Notifikasi::findOrFail($id);
        $notifikasi->update([
            'readed' => true,
        ]);
        if ($notifikasi->kategori == 'pengajuan') {
            return redirect()->route('user.pengajuan.show', $notifikasi->no_pengajuan);
        } elseif ($notifikasi->kategori == 'chat') {
            return redirect()->route('user.chat.show', $notifikasi->no_pengajuan);
        }
    }
}
