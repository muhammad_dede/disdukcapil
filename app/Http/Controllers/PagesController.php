<?php

namespace App\Http\Controllers;

use App\Mail\SendMessage;
use App\Models\Layanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function showLayanan()
    {
        return view('layanan');
    }

    public function detailLayanan($url)
    {
        $layanan = Layanan::with(['layananDokumen', 'layananPersyaratan', 'layananIsian'])->where('url', $url)->first();
        if (!$layanan) {
            return abort('404');
        }
        return view('layanan-detail', compact('layanan'));
    }

    public function showTanyaJawab()
    {
        return view('tanya-jawab');
    }

    public function showKontakKami()
    {
        return view('kontak-kami');
    }

    public function sendMessage(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'subject' => 'required|string|max:255',
            'message' => 'required|string|max:255',
        ]);

        $data = array(
            'name'      =>  $request->name,
            'email'     =>  $request->email,
            'subject'   =>  $request->subject,
            'message'   =>  $request->message,
        );

        Mail::to('bang.chyto@gmail.com')->send(new SendMessage($data));
        return back()->with('success', 'Terima kasih sudah mengirim pesan kepada kami!');
    }
}
