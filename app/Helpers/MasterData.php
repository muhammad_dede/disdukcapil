<?php

function get_counter()
{
    $data = [
        'warga' => App\Models\User::count(),
        'pengajuan' => App\Models\Pengajuan::count(),
        'proses' => App\Models\Pengajuan::where('status', '!=', '5')->count(),
        'selesai' => App\Models\Pengajuan::where('status', '5')->count(),
    ];
    return $data;
}

function get_layanan()
{
    return App\Models\Layanan::with(['layananDokumen', 'layananPersyaratan', 'layananIsian'])->where('aktif', true)->orderBy('id', 'asc')->get();
}

function get_status_pengajuan()
{
    return App\Models\MasterData\StatusPengajuan::orderBy('kode', 'asc')->get();
}

function get_riwayat_pengajuan_terbaru($no_pengajuan)
{
    return App\Models\PengajuanRiwayat::where('no_pengajuan', $no_pengajuan)->orderBy('created_at', 'desc')->first();
}

function get_jenis_kelamin_all()
{
    return App\Models\MasterData\JenisKelamin::all();
}

function get_golongan_darah()
{
    return App\Models\MasterData\GolonganDarah::orderBy('nama', 'asc')->get();
}

function get_rt()
{
    $data = [];

    for ($x = 1; $x < 301; $x++) {
        $data[] = [
            'rt' => sprintf("%02s", $x)
        ];
    }

    return $data;
}

function get_rw()
{
    $data = [];

    for ($x = 1; $x < 301; $x++) {
        $data[] = [
            'rw' => sprintf("%02s", $x)
        ];
    }

    return $data;
}

function get_kelurahan_cilegon()
{
    $data = [];
    $data_kecamatan = App\Models\MasterData\Kecamatan::where('kode_kota', '3672')->get();
    foreach ($data_kecamatan as $kecamatan) {
        $data_kelurahan = App\Models\MasterData\Kelurahan::where('kode_kecamatan', $kecamatan->kode)->get();
        foreach ($data_kelurahan as $kelurahan) {
            $data[] = [
                'kode' => $kelurahan->kode,
                'nama' => $kelurahan->nama,
                'kode_kecamatan' => $kelurahan->kode_kecamatan,
            ];
        }
    }
    return $data;
}

function get_kecamatan_cilegon()
{
    $data = [];
    $data_kecamatan = App\Models\MasterData\Kecamatan::where('kode_kota', '3672')->get();
    foreach ($data_kecamatan as $kecamatan) {
        $data[] = [
            'kode' => $kecamatan->kode,
            'nama' => $kecamatan->nama,
            'kode_kecamatan' => $kecamatan->kode_kota,
        ];
    }
    return $data;
}

function get_negara_all()
{
    $all_data = \App\Models\MasterData\Negara::all();
    return $all_data;
}

function get_pendidikan_all()
{
    $all_data = \App\Models\MasterData\Pendidikan::all();
    return $all_data;
}

function get_pekerjaan_all()
{
    $all_data = \App\Models\MasterData\Pekerjaan::all();
    return $all_data;
}

function get_agama_all()
{
    $all_data = \App\Models\MasterData\Agama::all();
    return $all_data;
}

function get_tempat_dilahirkan_all()
{
    $all_data = \App\Models\MasterData\TempatDilahirkan::all();
    return $all_data;
}

function get_hari_all()
{
    $all_data = \App\Models\MasterData\Hari::all();
    return $all_data;
}

function get_jenis_kelahiran_all()
{
    $all_data = \App\Models\MasterData\JenisKelahiran::all();
    return $all_data;
}

function get_tenaga_ahli_all()
{
    $all_data = \App\Models\MasterData\TenagaAhli::all();
    return $all_data;
}

function get_yang_menentukan_mati_all()
{
    $all_data = \App\Models\MasterData\YangMenentukanMati::all();
    return $all_data;
}

function get_status_perkawinan_sebelum_kawin_all()
{
    $all_data = \App\Models\MasterData\statusKawin::all();
    return $all_data;
}

function get_pengajuan_saya($status)
{
    return App\Models\Pengajuan::with(['pengajuanUpload', 'pengajuanRiwayat'])->where('id_warga', auth()->id())->where('status', $status)->orderBy('created_at', 'desc')->get();
}
