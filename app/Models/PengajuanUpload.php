<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanUpload extends Model
{
    use HasFactory;

    protected $table = 'pengajuan_upload';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $guarded = [];

    public function pengajuan()
    {
        return $this->belongsTo(Pengajuan::class, 'no_pengajuan', 'no_pengajuan');
    }

    public function persyaratan()
    {
        return $this->belongsTo(LayananPersyaratan::class, 'id_persyaratan', 'id');
    }
}
