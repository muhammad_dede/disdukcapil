<?php

namespace App\Models\DataPengajuan;

use App\Models\MasterData\GolonganDarah;
use App\Models\MasterData\JenisKelamin;
use App\Models\MasterData\Kecamatan;
use App\Models\MasterData\Kelurahan;
use App\Models\MasterData\Kota;
use App\Models\MasterData\Negara;
use App\Models\MasterData\Provinsi;
use App\Models\Pengajuan;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KtpElektronikBaru extends Model
{
    use HasFactory;

    protected $table = 'data_ktp_elektronik_baru';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $guarded = [];

    public function pengajuan()
    {
        return $this->morphOne(Pengajuan::class, 'data_pengajuan');
    }

    public function jenisKelaminKTP()
    {
        return $this->belongsTo(JenisKelamin::class, 'jenis_kelamin', 'kode');
    }

    public function golonganDarahKTP()
    {
        return $this->belongsTo(GolonganDarah::class, 'gol_darah', 'kode');
    }

    public function kewargaNegaraanKTP()
    {
        return $this->belongsTo(Negara::class, 'kewarganegaraan', 'kode');
    }

    public function kelurahanKTP()
    {
        return $this->belongsTo(Kelurahan::class, 'kelurahan', 'kode');
    }

    public function kecamatanKTP()
    {
        return $this->belongsTo(Kecamatan::class, 'kecamatan', 'kode');
    }

    public function kotaKTP()
    {
        return $this->belongsTo(Kota::class, 'kota', 'kode');
    }

    public function provinsiKTP()
    {
        return $this->belongsTo(Provinsi::class, 'provinsi', 'kode');
    }
}
