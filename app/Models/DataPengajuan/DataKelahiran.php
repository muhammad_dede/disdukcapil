<?php

namespace App\Models;

use App\Models\MasterData\Hari;
use App\Models\MasterData\JenisKelahiran;
use App\Models\MasterData\JenisKelamin;
use App\Models\MasterData\Negara;
use App\Models\MasterData\TempatDilahirkan;
use App\Models\MasterData\TenagaAhli;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataKelahiran extends Model
{
    use HasFactory;

    protected $table = 'data_kelahiran';
    protected $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    protected $guarded = [];

    public function pengajuan()
    {
        return $this->morphOne(Pengajuan::class, 'data_pengajuan');
    }

    public function tempatDilahirkanAnak()
    {
        return $this->belongsTo(TempatDilahirkan::class, 'tmpt_dilahirkan_anak', 'kode');
    }

    public function hariLahirAnak()
    {
        return $this->belongsTo(Hari::class, 'hari_lahir_anak', 'kode');
    }

    public function jenisKelahiranAnak()
    {
        return $this->belongsTo(JenisKelahiran::class, 'jenis_kelahiran_anak', 'kode');
    }

    public function penolongKelahiranAnak()
    {
        return $this->belongsTo(TenagaAhli::class, 'penolong_kelahiran_anak', 'kode');
    }

    public function kewarganegaraanAyah()
    {
        return $this->belongsTo(Negara::class, 'kewarganegaraan_ayah', 'kode');
    }

    public function kewarganegaraanIbu()
    {
        return $this->belongsTo(Negara::class, 'kewarganegaraan_ibu', 'kode');
    }

    public function kewarganegaraanSaksi1()
    {
        return $this->belongsTo(Negara::class, 'kewarganegaraan_saksi_1', 'kode');
    }

    public function kewarganegaraanSaksi2()
    {
        return $this->belongsTo(Negara::class, 'kewarganegaraan_saksi_2', 'kode');
    }

    public function kewarganegaraan()
    {
        return $this->belongsTo(Negara::class, 'kewarganegaraan', 'kode');
    }

    public function jenisKelaminAnak()
    {
        return $this->belongsTo(JenisKelamin::class, 'jenis_kelamin_anak', 'kode');
    }
}
