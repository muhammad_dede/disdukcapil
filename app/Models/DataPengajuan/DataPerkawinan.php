<?php

namespace App\Models;

use App\Models\MasterData\statusKawin;
use App\Models\MasterData\Agama;
use App\Models\MasterData\Negara;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPerkawinan extends Model
{
    use HasFactory;

    protected $table = 'data_perkawinan';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $guarded = [];

    public function pengajuan()
    {
        return $this->morphOne(Pengajuan::class, 'data_pengajuan');
    }

    public function statusKawin()
    {
        return $this->belongsTo(statusKawin::class, 'status_perkawinan_sebelum_kawin', 'kode');
    }

    public function agama()
    {
        return $this->belongsTo(Agama::class, 'agama_kepercayaan', 'kode');
    }

    public function kewarganegaraan()
    {
        return $this->belongsTo(Negara::class, 'kewarganegaraan', 'kode');
    }
}
