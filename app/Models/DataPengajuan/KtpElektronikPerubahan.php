<?php

namespace App\Models\DataPengajuan;

use App\Models\MasterData\Agama;
use App\Models\MasterData\Pekerjaan;
use App\Models\MasterData\Pendidikan;
use App\Models\Pengajuan;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KtpElektronikPerubahan extends Model
{
    use HasFactory;

    protected $table = 'data_ktp_elektronik_perubahan';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $guarded = [];

    public function pengajuan()
    {
        return $this->morphOne(Pengajuan::class, 'data_pengajuan');
    }

    public function pendidikanTerakhirSemula()
    {
        return $this->belongsTo(Pendidikan::class, 'pendidikan_terakhir_semula', 'kode');
    }

    public function pendidikanTerakhirMenjadi()
    {
        return $this->belongsTo(Pendidikan::class, 'pendidikan_terakhir_menjadi', 'kode');
    }

    public function pekerjaanSemula()
    {
        return $this->belongsTo(Pekerjaan::class, 'pekerjaan_semula', 'kode');
    }

    public function pekerjaanMenjadi()
    {
        return $this->belongsTo(Pekerjaan::class, 'pekerjaan_menjadi', 'kode');
    }

    public function agamaSemula()
    {
        return $this->belongsTo(Agama::class, 'agama_semula', 'kode');
    }

    public function agamaMenjadi()
    {
        return $this->belongsTo(Agama::class, 'agama_menjadi', 'kode');
    }
}
