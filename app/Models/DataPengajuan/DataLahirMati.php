<?php

namespace App\Models;


use App\Models\MasterData\JenisKelahiran;
use App\Models\MasterData\JenisKelamin;
use App\Models\MasterData\Negara;
use App\Models\MasterData\TempatDilahirkan;
use App\Models\MasterData\TenagaAhli;
use App\Models\MasterData\YangMenentukanMati;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataLahirMati extends Model
{
    use HasFactory;

    protected $table = 'data_lahir_mati';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $guarded = [];

    public function pengajuan()
    {
        return $this->morphOne(Pengajuan::class, 'data_pengajuan');
    }

    public function tempatDilahirkan()
    {
        return $this->belongsTo(TempatDilahirkan::class, 'tempat_dilahirkan', 'kode');
    }

    public function jenisKelahiran()
    {
        return $this->belongsTo(JenisKelahiran::class, 'jenis_kelahiran', 'kode');
    }

    public function penolongKelahiran()
    {
        return $this->belongsTo(TenagaAhli::class, 'penolong_kelahiran', 'kode');
    }

    public function kewarganegaraanAyah()
    {
        return $this->belongsTo(Negara::class, 'kewarganegaraan_ayah', 'kode');
    }

    public function kewarganegaraanIbu()
    {
        return $this->belongsTo(Negara::class, 'kewarganegaraan_ibu', 'kode');
    }

    public function kewarganegaraanSaksi1()
    {
        return $this->belongsTo(Negara::class, 'kewarganegaraan_saksi_1', 'kode');
    }

    public function kewarganegaraanSaksi2()
    {
        return $this->belongsTo(Negara::class, 'kewarganegaraan_saksi_2', 'kode');
    }

    public function kewarganegaraan()
    {
        return $this->belongsTo(Negara::class, 'kewarganegaraan', 'kode');
    }

    public function jenisKelamin()
    {
        return $this->belongsTo(JenisKelamin::class, 'jenis_kelamin', 'kode');
    }

    public function yangMenentukanMati()
    {
        return $this->belongsTo(YangMenentukanMati::class, 'yang_menentukan', 'kode');
    }
}
