<?php

namespace App\Models;

use App\Models\MasterData\StatusPengajuan;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
    use HasFactory;

    protected $table = 'pengajuan';
    protected $primaryKey = 'no_pengajuan';
    public $incrementing = false;

    protected $guarded = [];

    public function data_pengajuan()
    {
        return $this->morphTo(__FUNCTION__, 'data_pengajuan_type', 'data_pengajuan_id');
    }

    public function warga()
    {
        return $this->belongsTo(User::class, 'id_warga', 'id');
    }

    public function layanan()
    {
        return $this->belongsTo(Layanan::class, 'id_layanan', 'id');
    }

    public function statusPengajuan()
    {
        return $this->belongsTo(StatusPengajuan::class, 'status', 'kode');
    }

    public function pengajuanUpload()
    {
        return $this->hasMany(PengajuanUpload::class, 'no_pengajuan', 'no_pengajuan');
    }

    public function pengajuanChat()
    {
        return $this->hasMany(PengajuanChat::class, 'no_pengajuan', 'no_pengajuan');
    }

    public function pengajuanRiwayat()
    {
        return $this->hasMany(PengajuanRiwayat::class, 'no_pengajuan', 'no_pengajuan');
    }
}
