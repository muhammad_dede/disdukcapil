<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notifikasi extends Model
{
    use HasFactory;

    protected $table = 'notifikasi';
    protected $primaryKey = 'id';

    protected $guarded = [];

    public function pengajuan()
    {
        return $this->belongsTo(Pengajuan::class, 'no_pengajuan', 'no_pengajuan');
    }
}
