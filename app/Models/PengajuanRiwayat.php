<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanRiwayat extends Model
{
    use HasFactory;

    protected $table = 'pengajuan_riwayat';
    protected $primaryKey = 'id';

    protected $guarded = [];

    public function pengajuan()
    {
        return $this->belongsTo(Pengajuan::class, 'no_pengajuan', 'no_pengajuan');
    }
}
