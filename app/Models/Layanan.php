<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Layanan extends Model
{
    use HasFactory;

    protected $table = 'layanan';

    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $guarded = [];

    public function layananDokumen()
    {
        return $this->hasMany(LayananDokumen::class, 'id_layanan', 'id');
    }

    public function layananIsian()
    {
        return $this->hasOne(LayananIsian::class, 'id_layanan', 'id');
    }

    public function layananPersyaratan()
    {
        return $this->hasMany(LayananPersyaratan::class, 'id_layanan', 'id');
    }
}
