<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LayananDokumen extends Model
{
    use HasFactory;

    protected $table = 'layanan_dokumen';

    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $guarded = [];

    public function layanan()
    {
        return $this->belongsTo(Layanan::class, 'id_layanan', 'id');
    }
}
