@extends('layouts.app')

@section('title', 'Pengajuan Baru')

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Pengajuan Baru</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Pengajuan Baru</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="content" class="section-padding">
        <div class="container">
            <div class="row page-content">
                {{-- Card 1 --}}
                <div class="col-md-12 col-lg-12 col-xs-12">
                    <div class="services-item rounded" style="background-color: #6258C6;">
                        <div class="icon">
                            <i class="lni-bookmark-alt"></i>
                        </div>
                        <div class="services-content">
                            <h3><a href="#" class="text-white">{{ $layanan->layanan }}</a></h3>
                            <p class="text-white">{{ $layanan->keterangan }}</p>
                        </div>
                    </div>
                </div>
                @if ($errors->any())
                    <div class="col-md-12 col-lg-12 col-xs-12">
                        <div class="alert alert-danger">
                            <strong>WARNING: PERIKSA FORM ISIAN/PERSYARATAN PENGAJUAN.</strong>
                        </div>
                    </div>
                @endif
                @if (Session::has('failed'))
                    <div class="col-md-12 col-lg-12 col-xs-12">
                        <div class="alert alert-danger">
                            <strong>WARNING: {{ Session::get('failed') }}.</strong>
                        </div>
                    </div>
                @endif
                {{-- Card 2 --}}
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="inner-box">
                        <div class="tg-contactdetail">
                            <div class="dashboard-box">
                                <h2 class="dashbord-title">Form Isian</h2>
                            </div>
                            <div class="dashboard-wrapper">
                                <form action="{{ route('user.pengajuan.store', $layanan->url) }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="alert alert-warning">
                                        <ol>
                                            <li>
                                                <span>
                                                    Pengajuan layanan hanya berlaku untuk penduduk atau warga Kota
                                                    Cilegon
                                                </span>
                                            </li>
                                            <li>
                                                <span>Inputan dengan <sup class="text-danger">*</sup> wajib diisi.
                                                </span>
                                            </li>
                                        </ol>
                                    </div>
                                    @if ($layanan->id == 1)
                                        @include('pengajuan.create-form.1')
                                    @elseif($layanan->id == 2)
                                        @include('pengajuan.create-form.2')
                                    @elseif ($layanan->id == 3)
                                        @include('pengajuan.create-form.3')
                                    @elseif ($layanan->id == 4)
                                        @include('pengajuan.create-form.4')
                                    @elseif ($layanan->id == 5)
                                        @include('pengajuan.create-form.5')
                                    @elseif ($layanan->id == 6)
                                        @include('pengajuan.create-form.6')
                                    @endif
                                    @if ($layanan->layananPersyaratan)
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5>File Persyaratan Yang Harus Diupload</h5>
                                            </div>
                                            @foreach ($layanan->layananPersyaratan as $index => $persyaratan)
                                                <div class="col-md-12">
                                                    <input type="hidden" name="berkas[{{ $index }}][id_persyaratan]"
                                                        value="{{ $persyaratan->id }}">
                                                    <input type="hidden" name="berkas[{{ $index }}][wajib]"
                                                        value="{{ $persyaratan->wajib }}">
                                                    <div class="form-group mb-3">
                                                        <label class="control-label" for="kode_pos">
                                                            {{ $persyaratan->persyaratan }}
                                                            @if ($persyaratan->wajib)
                                                                <sup class="text-danger">*</sup>
                                                            @endif
                                                        </label>
                                                        <input type="file" name="berkas[{{ $index }}][file]"
                                                            id="file"
                                                            class="form-control input-md {{ $errors->has('berkas.' . $index . '.file') ? 'is-invalid' : '' }}"
                                                            accept="image/*">
                                                        @if ($errors->has('berkas.' . $index . '.file'))
                                                            <small class="text-danger">
                                                                {{ $errors->first('berkas.' . $index . '.file') }}
                                                            </small>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                    <div class="tg-checkbox mb-4">
                                        <div class="custom-control custom-checkbox">
                                            <input name="terms" type="checkbox"
                                                class="custom-control-input @error('terms') is-invalid @enderror"
                                                id="tg-agreetermsandrules">
                                            <label class="custom-control-label" for="tg-agreetermsandrules">
                                                Dengan ini Anda menyetujui syarat dan ketentuan yang berlaku.
                                                <a href="javascript:void(0);">
                                                    Terms &amp; Conditions
                                                </a>
                                            </label>
                                        </div>
                                        @error('terms')
                                            <br>
                                            <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <button class="btn btn-common btn-block mb-4" type="submit">Kirim Pengajuan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
