@extends('layouts.app')

@section('title', 'Pengajuan Saya')

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Pengajuan Saya</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Pengajuan Saya</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="section-padding">
        <div class="container">
            <div class="row">
                @if (Session::has('success'))
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ Session::get('success') }}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
                <div class="col-sm-12 col-md-4 col-lg-3 page-sidebar">
                    @include('layouts.aside')
                </div>
                <div class="col-sm-12 col-md-8 col-lg-9">
                    <div class="page-content">
                        <div class="inner-box">
                            <div class="dashboard-box">
                                <h2 class="dashbord-title">Pengajuan Saya</h2>
                            </div>
                            <div class="dashboard-wrapper">
                                <nav class="nav-table">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        @foreach (get_status_pengajuan() as $status_pengajuan)
                                            <li class="nav-item">
                                                <a class="nav-link {{ $status_pengajuan->kode == '1' ? 'active' : '' }}"
                                                    id="{{ $status_pengajuan->kode }}-tab" data-toggle="tab"
                                                    href="#{{ $status_pengajuan->kode }}" role="tab"
                                                    aria-controls="{{ $status_pengajuan->kode }}" aria-selected="true">
                                                    <span>
                                                        {{ strtoupper($status_pengajuan->nama) }}
                                                    </span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </nav>
                                <div class="tab-content" id="myTabContent">
                                    @foreach (get_status_pengajuan() as $status_pengajuan)
                                        <div class="tab-pane fade {{ $status_pengajuan->kode == '1' ? 'show active' : '' }}"
                                            id="{{ $status_pengajuan->kode }}" role="tabpanel"
                                            aria-labelledby="{{ $status_pengajuan->kode }}-tab">
                                            @if (get_pengajuan_saya($status_pengajuan->kode)->count() > 0)
                                                <table class="table table-responsive dashboardtable tablemyads">
                                                    <thead>
                                                        <tr style="width: 100%;">
                                                            <th class="align-middle">NO</th>
                                                            <th class="align-middle" style="width: 50%;">LAYANAN</th>
                                                            <th class="align-middle" style="width: 20%;">TANGGAL</th>
                                                            <th class="align-middle" style="width: 25%;">ACTION
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach (get_pengajuan_saya($status_pengajuan->kode) as $pengajuan)
                                                            <tr>
                                                                <td>{{ $loop->iteration }}</td>
                                                                <td>
                                                                    <h3>{{ $pengajuan->layanan->layanan }}</h3>
                                                                    @if (get_riwayat_pengajuan_terbaru($pengajuan->no_pengajuan))
                                                                        <span
                                                                            class="text-info">{{ get_riwayat_pengajuan_terbaru($pengajuan->no_pengajuan)->keterangan }}</span>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <span>
                                                                        {{ \Carbon\Carbon::parse($pengajuan->created_at)->isoFormat('D MMMM Y') }}
                                                                    </span>
                                                                </td>
                                                                <td>
                                                                    <div class="btns-actions">
                                                                        @if ($pengajuan->status !== '3')
                                                                            <a class="btn-action btn-view"
                                                                                href="{{ route('user.chat.show', $pengajuan->no_pengajuan) }}"
                                                                                title="Hubungi Operator">
                                                                                <i class="lni-bubble"></i>
                                                                            </a>
                                                                        @endif
                                                                        <a class="btn-action btn-edit"
                                                                            href="{{ route('user.pengajuan.show', $pengajuan->no_pengajuan) }}"
                                                                            title="Detail">
                                                                            <i class="lni-eye"></i>
                                                                        </a>
                                                                        @if ($pengajuan->status == '1')
                                                                            <a class="btn-action btn-delete btn_cancel"
                                                                                href="{{ route('user.pengajuan.cancel', $pengajuan->no_pengajuan) }}"
                                                                                title="Batalkan"
                                                                                id="{{ $pengajuan->no_pengajuan }}">
                                                                                <i class="lni-power-switch"></i>
                                                                            </a>
                                                                            <form
                                                                                action="{{ route('user.pengajuan.cancel', $pengajuan->no_pengajuan) }}"
                                                                                method="POST"
                                                                                id="form_{{ $pengajuan->no_pengajuan }}"
                                                                                class="d-none form_cancel">
                                                                                @csrf
                                                                            </form>
                                                                        @endif
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            @else
                                                <div class="p-5 text-center">
                                                    <img src="{{ asset('') }}assets/img/no-data.png"
                                                        class="img-fluid" alt="no-data">
                                                    <br>
                                                    <span>BELUM ADA PENGAJUAN</span>
                                                </div>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('my-js')
    <script>
        $(document).ready(function() {
            $(".btn_cancel").on("click", function(event) {
                event.preventDefault();
                Swal.fire({
                    title: 'Batalkan Pengajuan?',
                    text: "Anda akan membatalkan pengajuan ini!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#form_' + $(this).attr('id')).submit();
                    }
                })
            });
        });
    </script>
@endpush
