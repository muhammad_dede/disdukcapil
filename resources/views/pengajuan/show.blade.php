@extends('layouts.app')

@section('title', 'Detail Pengajuan')

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Detail Pengajuan</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Detail Pengajuan</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="section-padding">
        <div class="container">
            <div class="row">
                @if (Session::has('success'))
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ Session::get('success') }}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
                <div class="col-sm-12 col-md-4 col-lg-3 page-sidebar">
                    @include('layouts.aside')
                </div>
                <div class="col-sm-12 col-md-8 col-lg-9">
                    <div class="page-content">
                        <div class="inner-box">
                            <div class="dashboard-box">
                                <h2 class="dashbord-title">Detail Pengajuan</h2>
                            </div>
                            <div class="dashboard-wrapper">
                                <div class="card card--padding fill-bg mb-4">
                                    <table class="table table-responsive table-borderless">
                                        <tbody>
                                            <tr>
                                                <th>Jenis Pengajuan</th>
                                                <th>{{ $pengajuan->layanan->layanan }}</th>
                                            </tr>
                                            <tr>
                                                <th>Tanggal Pengajuan</th>
                                                <th>{{ \Carbon\Carbon::parse($pengajuan->created_at)->isoFormat('D MMMM Y') }}
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>Keterangan</th>
                                                <th>{{ get_riwayat_pengajuan_terbaru($pengajuan->no_pengajuan) ? get_riwayat_pengajuan_terbaru($pengajuan->no_pengajuan)->keterangan : '-' }}
                                                </th>
                                            </tr>
                                        </tbody>
                                    </table>
                                    @if ($pengajuan->status == '1')
                                        <a href="{{ route('user.pengajuan.cancel', $pengajuan->no_pengajuan) }}"
                                            class="btn btn-danger my-1 btn_cancel">Batalkan Pengajuan</a>
                                        <form action="{{ route('user.pengajuan.cancel', $pengajuan->no_pengajuan) }}"
                                            method="POST" class="d-none form_cancel">
                                            @csrf
                                        </form>
                                    @endif
                                    @if ($pengajuan->status !== '3')
                                        <a href="#" class="btn btn-info my-1">Hubungi Operator</a>
                                    @endif
                                    <a href="{{ route('user.pengajuan.index') }}" class="btn btn-light my-1">Kembali</a>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-sx-12">
                                        <div class="order-details">
                                            <div class="dashboardboxtitle">
                                                <h2>Informasi Data Pengajuan</h2>
                                            </div>
                                            @if ($pengajuan->id_layanan == 1)
                                                @include('pengajuan.show.1')
                                            @elseif ($pengajuan->id_layanan == 2)
                                                @include('pengajuan.show.2')
                                            @elseif ($pengajuan->id_layanan == 3)
                                                @include('pengajuan.show.3')
                                            @elseif ($pengajuan->id_layanan == 4)
                                                @include('pengajuan.show.4')
                                            @elseif ($pengajuan->id_layanan == 5)
                                                @include('pengajuan.show.5')
                                            @elseif ($pengajuan->id_layanan == 6)
                                                @include('pengajuan.show.6')
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-sx-12">
                                        <div class="dashboardboxtitle">
                                            <h2>Data Persyaratan Yang Diupload</h2>
                                        </div>

                                        <div class="order_review mb-3">
                                            @if ($pengajuan->pengajuanUpload->count() > 0)
                                                <table class="table table-responsive dashboardtable table-review-order"
                                                    style="width: 100%;">
                                                    <tbody>
                                                        @foreach ($pengajuan->pengajuanUpload as $pengajuan_upload)
                                                            <tr>
                                                                <td>
                                                                    <p>{{ $pengajuan_upload->persyaratan->persyaratan }}
                                                                    </p>
                                                                </td>
                                                                <td>
                                                                    <p>
                                                                        <a href="{{ asset('') }}upload-persyaratan/{{ $pengajuan_upload->file }}"
                                                                            target="_blank">
                                                                            LIHAT
                                                                        </a>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            @else
                                                <div class="p-4 text-center">
                                                    <img src="{{ asset('') }}assets/img/no-data.png"
                                                        class="img-fluid" alt="no-data">
                                                    <br>
                                                    <span>TANPA PERSYARATAN</span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('my-js')
    <script>
        $(document).ready(function() {
            $(".btn_cancel").on("click", function(event) {
                event.preventDefault();
                Swal.fire({
                    title: 'Batalkan Pengajuan?',
                    text: "Anda akan membatalkan pengajuan ini!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(".form_cancel").submit();
                    }
                })
            });
        });
    </script>
@endpush
