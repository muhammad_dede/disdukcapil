<div class="order_review mb-3">
    <table class="table table-responsive dashboardtable table-review-order" style="width: 100%;">
        <tbody>
            <tr>
                <td>
                    <p>Nama Lengkap Pelapor</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_pelapor }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Kartu Keluarga</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nomor_kartu_keluarga }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan ? $pengajuan->data_pengajuan->Kewarganegaraan->nama : '-' }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Lamanya Dalam Kandungan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->lamanya_dalam_kandungan }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Jenis Kelamin</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->jenis_kelamin ? $pengajuan->data_pengajuan->jenisKelamin->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Lahir Mati</p>
                </td>
                <td>
                    <p>
                        {{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_lahir_mati)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Jenis Kelahiran</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->jenis_kelahiran ? $pengajuan->data_pengajuan->jenisKelahiran->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Anak ke</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->anak_ke }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tempat dilahirkan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->tempat_dilahirkan ? $pengajuan->data_pengajuan->tempatDilahirkan->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Penolong Kelahiran</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->penolong_kelahiran ? $pengajuan->data_pengajuan->penolongKelahiran->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Sebab Lahir Mati</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->sebab_lahir_mati }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Yang Menentukan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->yang_menentukan ? $pengajuan->data_pengajuan->yangMenentukanMati->nama : '-' }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Tempat Kelahiran</p>
                </td>
                <td>
                    <p>
                        {{ $pengajuan->data_pengajuan->tempat_kelahiran }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Nama Ayah</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_ayah }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK Ayah</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik_ayah }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tempat Lahir Ayah</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->tempat_lahir_ayah }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Lahir Ayah</p>
                </td>
                <td>
                    <p>{{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_lahir_ayah)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan_ayah ? $pengajuan->data_pengajuan->kewarganegaraanAyah->nama : '-' }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Nama Ibu</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_ibu }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK Ibu</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik_ibu }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tempat Lahir Ibu</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->tempat_lahir_ibu }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Lahir Ibu</p>
                </td>
                <td>
                    <p>{{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_lahir_ibu)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan_ibu ? $pengajuan->data_pengajuan->kewarganegaraanIbu->nama : '-' }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Nama Lengkap Saksi 1</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_saksi_1 }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik_saksi_1 }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Kartu Keluarga</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->no_kk_saksi_1 }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan_saksi_1 ? $pengajuan->data_pengajuan->kewarganegaraanSaksi1->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nama Lengkap Saksi 2</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_saksi_2 }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik_saksi_2 }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Kartu Keluarga</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->no_kk_saksi_2 }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan_saksi_2 ? $pengajuan->data_pengajuan->kewarganegaraanSaksi2->nama : '-' }}
                    </p>
                </td>
            </tr>

        </tbody>
    </table>
</div>
