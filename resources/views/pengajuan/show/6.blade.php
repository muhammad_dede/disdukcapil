<div class="order_review mb-3">
    <table class="table table-responsive dashboardtable table-review-order" style="width: 100%;">
        <tbody>
            <tr>
                <td>
                    <p>Nama Lengkap Pelapor</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_pelapor }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Kartu Keluarga</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nomor_kartu_keluarga }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan ? $pengajuan->data_pengajuan->Kewarganegaraan->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Perkawinan</p>
                </td>
                <td>
                    <p>{{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_perkawinan)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Akta Perkawinan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->no_akta_perkawinan }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Akta Perkawinan</p>
                </td>
                <td>
                    <p>{{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_akta_perkawinan)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nama Pengadilan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_pengadilan_pembatalan }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Putusan Pengadilan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->no_putusan_pengadilan }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Putusan Pengadilan</p>
                </td>
                <td>
                    <p>{{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_putusan_pengadilan)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Pelaporan Perkawinan diluar Negeri</p>
                </td>
                <td>
                    <p>{{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_pelaporan_perkawinan_luar_negeri)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>

        </tbody>
    </table>
</div>
