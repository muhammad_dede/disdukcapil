<div class="order_review mb-3">
    <table class="table table-responsive dashboardtable table-review-order" style="width: 100%;">
        <tbody>
            <tr>
                <td>
                    <p>Nama Lengkap</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_lengkap }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tempat Lahir</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->tempat_lahir }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Lahir</p>
                </td>
                <td>
                    <p>
                        {{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_lahir)->isoFormat('D MMMM Y') }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Jenis Kelamin</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->jenis_kelamin ? $pengajuan->data_pengajuan->jenisKelaminKTP->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nama Ibu</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_ibu }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Golongan Darah</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->gol_darah ? $pengajuan->data_pengajuan->golonganDarahKTP->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan ? $pengajuan->data_pengajuan->kewargaNegaraanKTP->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Alamat</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->alamat }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>RT</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->rt }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>RW</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->rw }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kelurahan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kelurahan ? $pengajuan->data_pengajuan->kelurahanKTP->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kecamatan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kecamatan ? $pengajuan->data_pengajuan->kecamatanKTP->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kota/Kabupaten</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kota ? $pengajuan->data_pengajuan->kotaKTP->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Provinsi</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->provinsi ? $pengajuan->data_pengajuan->provinsiKTP->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kode Pos</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kode_pos ? $pengajuan->data_pengajuan->kode_pos : '-' }}
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
