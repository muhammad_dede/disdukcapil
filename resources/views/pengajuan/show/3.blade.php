<div class="order_review mb-3">
    <table class="table table-responsive dashboardtable table-review-order" style="width: 100%;">
        <tbody>
            <tr>
                <td>
                    <p>Nama Lengkap Pelapor</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_pelapor }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Kartu Keluarga</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nomor_kartu_keluarga }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan ? $pengajuan->data_pengajuan->Kewarganegaraan->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nama Lengkap</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_anak }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Jenis Kelamin</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->jenis_kelamin_anak ? $pengajuan->data_pengajuan->jenisKelaminAnak->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tempat dilahirkan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->tmpt_dilahirkan_anak ? $pengajuan->data_pengajuan->tempatDilahirkanAnak->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tempat Kelahiran</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->tmpt_kelahiran_anak }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Hari Lahir</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->hari_lahir_anak ? $pengajuan->data_pengajuan->hariLahirAnak->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Lahir</p>
                </td>
                <td>
                    <p>
                        {{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_lahir_anak)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Waktu Lahir</p>
                </td>
                <td>
                    <p>
                        {{ $pengajuan->data_pengajuan->waktu_lahir_anak }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Jenis Kelahiran</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->jenis_kelahiran_anak ? $pengajuan->data_pengajuan->jenisKelahiranAnak->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kelahiran ke</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kelahiran_ke_anak }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Penolong Kelahiran</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->penolong_kelahiran_anak ? $pengajuan->data_pengajuan->penolongKelahiranAnak->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Berat Bayi</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->berat_bayi_anak }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Panjang Bayi</p>
                </td>
                <td>
                    <p>
                        {{ $pengajuan->data_pengajuan->panjang_bayi_anak }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Nama Ayah</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_ayah }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK Ayah</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik_ayah }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tempat Lahir Ayah</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->tempat_lahir_ayah }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Lahir Ayah</p>
                </td>
                <td>
                    <p>{{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_lahir_ayah)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan_ayah ? $pengajuan->data_pengajuan->kewarganegaraanAyah->nama : '-' }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Nama Ibu</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_ibu }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK Ibu</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik_ibu }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tempat Lahir Ibu</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->tempat_lahir_ibu }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Lahir Ibu</p>
                </td>
                <td>
                    <p>{{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_lahir_ibu)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan_ibu ? $pengajuan->data_pengajuan->kewarganegaraanIbu->nama : '-' }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Nama Lengkap Saksi 1</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_saksi_1 }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik_saksi_1 }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Kartu Keluarga</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->no_kk_saksi_1 }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan_saksi_1 ? $pengajuan->data_pengajuan->kewarganegaraanSaksi1->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nama Lengkap Saksi 2</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_saksi_2 }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik_saksi_2 }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Kartu Keluarga</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->no_kk_saksi_2 }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan_saksi_2 ? $pengajuan->data_pengajuan->kewarganegaraanSaksi2->nama : '-' }}
                    </p>
                </td>
            </tr>

        </tbody>
    </table>
</div>
