<div class="order_review mb-3">
    <table class="table table-responsive dashboardtable table-review-order" style="width: 100%;">
        <tbody>
            <tr>
                <td>
                    <p>Nama Lengkap Pelapor</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_pelapor }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Kartu Keluarga</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nomor_kartu_keluarga }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Kewarganegaraan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->kewarganegaraan ? $pengajuan->data_pengajuan->Kewarganegaraan->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK Ayah dari Suami</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik_ayah_dari_suami }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nama Ayah dari Suami</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_ayah_dari_suami }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK Ibu dari Suami</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik_ibu_dari_suami }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nama Ibu dari Suami</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_ibu_dari_suami }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK Ayah dari Istri</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik_ayah_dari_istri }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nama Ayah dari Istri</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_ayah_dari_istri }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK Ibu dari Istri</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik_ibu_dari_istri }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nama Ibu dari Istri</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_ibu_dari_istri }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Status Perkawinan Sebelum Kawin</p>
                </td>
                <td>
                    <p>
                        {{ $pengajuan->data_pengajuan->status_perkawinan_sebelum_kawin }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Perkawinan Yang ke-</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->perkawinan_yang_ke }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Istri Yang ke- (bagi yang poligami)</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->istri_yang_ke }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Pemberkatan Perkawinan</p>
                </td>
                <td>
                    <p>{{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_pemberkatan_perkawinan)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Melapor</p>
                </td>
                <td>
                    <p>{{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_melapor)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Jam Pelaporan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->jam_pelaporan }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Agama</p>
                </td>
                <td>
                    <p>
                        {{ $pengajuan->data_pengajuan->agama_kepercayaan ? $pengajuan->data_pengajuan->Agama->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nama Organisasi Kepercayaan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_organisasi_kepercayaan }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nama Pengadilan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_pengadilan }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Penetapan Pengadilan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->no_penetapan_pengadilan }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Penetapan Pengadilan</p>
                </td>
                <td>
                    <p>{{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_penepatan_pengadilan)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Nama Pemuka Agama</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_pemuka_agama }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Surat Izin dari Perwakilan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->no_surat_izin_dari_perwakilan }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor Passport</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->no_passport }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Perjanjian Perkawinan dibuat oleh Notaris</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->perjanjian_perkawinan }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Nomor Akta Notaris</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->no_akta_notaris }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal Akta Notaris</p>
                </td>
                <td>
                    <p>{{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->tgl_akta_notaris)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Jumlah Anak</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->jumlah_anak }}</p>
                </td>
            </tr>

        </tbody>
    </table>
</div>
