<div class="order_review mb-3">
    <table class="table table-responsive dashboardtable table-review-order" style="width: 100%;">
        <tbody>
            <tr>
                <td>
                    <p>Nama Lengkap</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nama_lengkap }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>NIK</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->nik }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Alamat Rumah</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->alamat_rumah }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Pendidikan Terakhir Semula</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->pendidikan_terakhir_semula ? $pengajuan->data_pengajuan->pendidikanTerakhirSemula->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Pendidikan Terakhir Menjadi</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->pendidikan_terakhir_menjadi ? $pengajuan->data_pengajuan->pendidikanTerakhirMenjadi->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Dasar Perubahan Pendidikan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->pendidikan_terakhir_dasar_perubahan }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->pendidikan_terakhir_no }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal</p>
                </td>
                <td>
                    <p>
                        {{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->pendidikan_terakhir_tgl)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Pekerjaan Semula</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->pekerjaan_semula ? $pengajuan->data_pengajuan->pekerjaanSemula->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Pekerjaan Menjadi</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->pekerjaan_menjadi ? $pengajuan->data_pengajuan->pekerjaanMenjadi->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Dasar Perubahan Pekerjaan</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->pekerjaan_dasar_perubahan }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->pekerjaan_no }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal</p>
                </td>
                <td>
                    <p>
                        {{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->pekerjaan_tgl)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Agama Semula</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->agama_semula ? $pengajuan->data_pengajuan->agamaSemula->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Agama Menjadi</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->agama_menjadi ? $pengajuan->data_pengajuan->agamaMenjadi->nama : '-' }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Dasar Perubahan Agama</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->agama_dasar_perubahan }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->agama_no }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal</p>
                </td>
                <td>
                    <p>
                        {{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->agama_tgl)->isoFormat('D MMMM Y') }}</p>
                </td>
            </tr>

            <tr>
                <td>
                    <p>Perubahan Lainnya Semula</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->perubahan_lainnya_semula }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Perubahan Lainnya Menjadi</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->perubahan_lainnya_semula }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Dasar Perubahan Lainnya</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->perubahan_lainnya_dasar_perubahan }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Nomor</p>
                </td>
                <td>
                    <p>{{ $pengajuan->data_pengajuan->perubahan_lainnya_no }}
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Tanggal</p>
                </td>
                <td>
                    <p>
                        {{ \Carbon\Carbon::parse($pengajuan->data_pengajuan->perubahan_lainnya_tgl)->isoFormat('D MMMM Y') }}
                    </p>
                </td>
            </tr>

        </tbody>
    </table>
</div>
