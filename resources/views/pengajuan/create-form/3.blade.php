{{-- data pelapor --}}
<h5 class="mb-3">Data Pelapor</h5>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_pelapor">
                Nama Lengkap Pelapor
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_pelapor') is-invalid @enderror" name="nama_pelapor"
                type="text" id="nama_pelapor" value="{{ old('nama_pelapor') }}">
            @error('nama_pelapor')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik">
                NIK
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik') is-invalid @enderror" name="nik" type="text" id="nik"
                value="{{ old('nik') }}">
            @error('nik')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nomor_kartu_keluarga">
                Nomor Kartu Keluarga
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nomor_kartu_keluarga') is-invalid @enderror"
                name="nomor_kartu_keluarga" type="text" id="nomor_kartu_keluarga"
                value="{{ old('nomor_kartu_keluarga') }}">
            @error('nomor_kartu_keluarga')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="kewarganegaraan">
                Kewarganegaraan
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('kewarganegaraan') is-invalid @enderror" id="kewarganegaraan">
                <select name="kewarganegaraan">
                    <option value=""></option>
                    @foreach (get_negara_all() as $negara)
                        <option value="{{ $negara->kode }}"
                            {{ old('kewarganegaraan') == $negara->kode ? 'selected' : '' }}>
                            {{ $negara->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('kewarganegaraan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>

<hr>
<h5 class="mb-3">Data Anak</h5>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_anak">
                Nama Lengkap
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_anak') is-invalid @enderror" name="nama_anak" type="text"
                id="nama_anak" value="{{ old('nama_anak') }}">
            @error('nama_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="jenis_kelamin_anak">
                Jenis Kelamin
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('jenis_kelamin_anak') is-invalid @enderror"
                id="jenis_kelamin_anak">
                <select name="jenis_kelamin_anak">
                    <option value=""></option>
                    @foreach (get_jenis_kelamin_all() as $jenis_kelamin)
                        <option value="{{ $jenis_kelamin->kode }}"
                            {{ old('jenis_kelamin_anak') == $jenis_kelamin->kode ? 'selected' : '' }}>
                            {{ $jenis_kelamin->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('jenis_kelamin_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="tmpt_dilahirkan_anak">
                Tempat dilahirkan
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('tmpt_dilahirkan_anak') is-invalid @enderror"
                id="tmpt_dilahirkan_anak">
                <select name="tmpt_dilahirkan_anak">
                    <option value=""></option>
                    @foreach (get_tempat_dilahirkan_all() as $tempat_dilahirkan)
                        <option value="{{ $tempat_dilahirkan->kode }}"
                            {{ old('tmpt_dilahirkan_anak') == $tempat_dilahirkan->kode ? 'selected' : '' }}>
                            {{ $tempat_dilahirkan->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('tmpt_dilahirkan_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="tmpt_kelahiran_anak">
                Tempat Kelahiran
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tmpt_kelahiran_anak') is-invalid @enderror"
                name="tmpt_kelahiran_anak" type="text" id="tmpt_kelahiran_anak"
                value="{{ old('tmpt_kelahiran_anak') }}">
            @error('tmpt_kelahiran_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="hari_lahir_anak">
                Hari
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('hari_lahir_anak') is-invalid @enderror" id="hari_lahir_anak">
                <select name="hari_lahir_anak">
                    <option value=""></option>
                    @foreach (get_hari_all() as $hari)
                        <option value="{{ $hari->kode }}"
                            {{ old('hari_lahir_anak') == $hari->kode ? 'selected' : '' }}>
                            {{ $hari->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('hari_lahir_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="tgl_lahir_anak">
                Tanggal Lahir
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_lahir_anak') is-invalid @enderror" name="tgl_lahir_anak"
                type="date" id="tgl_lahir_anak" value="{{ old('tgl_lahir_anak') }}">
            @error('tgl_lahir_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="waktu_lahir_anak">
                Waktu Lahir
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('waktu_lahir_anak') is-invalid @enderror" name="waktu_lahir_anak"
                type="time" id="waktu_lahir_anak" value="{{ old('waktu_lahir_anak') }}">
            @error('waktu_lahir_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="jenis_kelahiran_anak">
                Jenis Kelahiran
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('jenis_kelahiran_anak') is-invalid @enderror"
                id="jenis_kelahiran_anak">
                <select name="jenis_kelahiran_anak">
                    <option value=""></option>
                    @foreach (get_jenis_kelahiran_all() as $jenis_kelahiran)
                        <option value="{{ $jenis_kelahiran->kode }}"
                            {{ old('jenis_kelahiran_anak') == $jenis_kelahiran->kode ? 'selected' : '' }}>
                            {{ $jenis_kelahiran->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('jenis_kelahiran_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="kelahiran_ke_anak">
                Kelahiran ke
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('kelahiran_ke_anak') is-invalid @enderror"
                name="kelahiran_ke_anak" type="text" id="kelahiran_ke_anak" value="{{ old('kelahiran_ke_anak') }}">
            @error('kelahiran_ke_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="penolong_kelahiran_anak">
                Penolong Kelahiran
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('penolong_kelahiran_anak') is-invalid @enderror"
                id="penolong_kelahiran_anak">
                <select name="penolong_kelahiran_anak">
                    <option value=""></option>
                    @foreach (get_tenaga_ahli_all() as $penolong_kelahiran_anak)
                        <option value="{{ $penolong_kelahiran_anak->kode }}"
                            {{ old('penolong_kelahiran_anak') == $penolong_kelahiran_anak->kode ? 'selected' : '' }}>
                            {{ $penolong_kelahiran_anak->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('penolong_kelahiran_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="berat_bayi_anak">
                Berat Bayi
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('berat_bayi_anak') is-invalid @enderror" name="berat_bayi_anak"
                type="text" id="berat_bayi_anak" value="{{ old('berat_bayi_anak') }}">
            @error('berat_bayi_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="panjang_bayi_anak">
                Panjang Bayi
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('panjang_bayi_anak') is-invalid @enderror"
                name="panjang_bayi_anak" type="text" id="panjang_bayi_anak" value="{{ old('panjang_bayi_anak') }}">
            @error('panjang_bayi_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>

{{-- data ortu --}}
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_ayah">
                Nama Ayah
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_ayah') is-invalid @enderror" name="nama_ayah" type="text"
                id="nama_ayah" value="{{ old('nama_ayah') }}">
            @error('nama_ayah')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik_ayah">
                NIK Ayah
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik_ayah') is-invalid @enderror" name="nik_ayah" type="text"
                id="nik_ayah" value="{{ old('nik_ayah') }}">
            @error('nik_ayah')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="tempat_lahir_ayah">
                Tempat Lahir Ayah
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tempat_lahir_ayah') is-invalid @enderror"
                name="tempat_lahir_ayah" type="text" id="tempat_lahir_ayah" value="{{ old('tempat_lahir_ayah') }}">
            @error('tempat_lahir_ayah')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="tgl_lahir_ayah">
                Tanggal Lahir Ayah
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_lahir_ayah') is-invalid @enderror" name="tgl_lahir_ayah"
                type="date" id="tgl_lahir_ayah" value="{{ old('tgl_lahir_ayah') }}">
            @error('tgl_lahir_ayah')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="kewarganegaraan_ayah">
                Kewarganegaraan Ayah
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('kewarganegaraan_ayah') is-invalid @enderror"
                id="kewarganegaraan_ayah">
                <select name="kewarganegaraan_ayah">
                    <option value=""></option>
                    @foreach (get_negara_all() as $negara)
                        <option value="{{ $negara->kode }}"
                            {{ old('kewarganegaraan_ayah') == $negara->kode ? 'selected' : '' }}>
                            {{ $negara->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('kewarganegaraan_ayah')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_ibu">
                Nama Ibu
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_ibu') is-invalid @enderror" name="nama_ibu" type="text"
                id="nama_ibu" value="{{ old('nama_ibu') }}">
            @error('nama_ibu')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik_ibu">
                NIK Ibu
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik_ibu') is-invalid @enderror" name="nik_ibu" type="text"
                id="nik_ibu" value="{{ old('nik_ibu') }}">
            @error('nik_ibu')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="tempat_lahir_ibu">
                Tempat Lahir Ibu
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tempat_lahir_ibu') is-invalid @enderror"
                name="tempat_lahir_ibu" type="text" id="tempat_lahir_ibu" value="{{ old('tempat_lahir_ibu') }}">
            @error('tempat_lahir_ibu')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="tgl_lahir_ibu">
                Tanggal Lahir Ibu
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_lahir_ibu') is-invalid @enderror" name="tgl_lahir_ibu"
                type="date" id="tgl_lahir_ibu" value="{{ old('tgl_lahir_ibu') }}">
            @error('tgl_lahir_ibu')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="kewarganegaraan_ibu">
                Kewarganegaraan Ibu
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('kewarganegaraan_ibu') is-invalid @enderror"
                id="kewarganegaraan_ibu">
                <select name="kewarganegaraan_ibu">
                    <option value=""></option>
                    @foreach (get_negara_all() as $negara)
                        <option value="{{ $negara->kode }}"
                            {{ old('kewarganegaraan_ibu') == $negara->kode ? 'selected' : '' }}>
                            {{ $negara->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('kewarganegaraan_ibu')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>

{{-- data saksi --}}
<hr>
<h5 class="mb-3">Data Saksi</h5>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_saksi_1">
                Nama Lengkap Saksi 1
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_saksi_1') is-invalid @enderror" name="nama_saksi_1"
                type="text" id="nama_saksi_1" value="{{ old('nama_saksi_1') }}">
            @error('nama_saksi_1')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik_saksi_1">
                NIK
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik_saksi_1') is-invalid @enderror" name="nik_saksi_1"
                type="text" id="nik_saksi_1" value="{{ old('nik_saksi_1') }}">
            @error('nik_saksi_1')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="no_kk_saksi_1">
                Nomor Kartu Keluarga
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('no_kk_saksi_1') is-invalid @enderror" name="no_kk_saksi_1"
                type="text" id="no_kk_saksi_1" value="{{ old('no_kk_saksi_1') }}">
            @error('no_kk_saksi_1')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="kewarganegaraan_saksi_1">
                Kewarganegaraan
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('kewarganegaraan_saksi_1') is-invalid @enderror"
                id="kewarganegaraan_saksi_1">
                <select name="kewarganegaraan_saksi_1">
                    <option value=""></option>
                    @foreach (get_negara_all() as $negara)
                        <option value="{{ $negara->kode }}"
                            {{ old('kewarganegaraan_saksi_1') == $negara->kode ? 'selected' : '' }}>
                            {{ $negara->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('kewarganegaraan_saksi_1')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_saksi_2">
                Nama Lengkap Saksi 2
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_saksi_2') is-invalid @enderror" name="nama_saksi_2"
                type="text" id="nama_saksi_2" value="{{ old('nama_saksi_2') }}">
            @error('nama_saksi_2')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik_saksi_2">
                NIK
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik_saksi_2') is-invalid @enderror" name="nik_saksi_2"
                type="text" id="nik_saksi_2" value="{{ old('nik_saksi_2') }}">
            @error('nik_saksi_2')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="no_kk_saksi_2">
                Nomor Kartu Keluarga
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('no_kk_saksi_2') is-invalid @enderror" name="no_kk_saksi_2"
                type="text" id="no_kk_saksi_2" value="{{ old('no_kk_saksi_2') }}">
            @error('no_kk_saksi_2')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="kewarganegaraan_saksi_2">
                Kewarganegaraan
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('kewarganegaraan_saksi_2') is-invalid @enderror"
                id="kewarganegaraan_saksi_2">
                <select name="kewarganegaraan_saksi_2">
                    <option value=""></option>
                    @foreach (get_negara_all() as $negara)
                        <option value="{{ $negara->kode }}"
                            {{ old('kewarganegaraan_saksi_2') == $negara->kode ? 'selected' : '' }}>
                            {{ $negara->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('kewarganegaraan_saksi_2')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>
