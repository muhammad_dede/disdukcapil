{{-- data pelapor --}}
<h5 class="mb-3">Data Pelapor</h5>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_pelapor">
                Nama Lengkap Pelapor
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_pelapor') is-invalid @enderror" name="nama_pelapor"
                type="text" id="nama_pelapor" value="{{ old('nama_pelapor') }}">
            @error('nama_pelapor')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik">
                NIK
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik') is-invalid @enderror" name="nik" type="text" id="nik"
                value="{{ old('nik') }}">
            @error('nik')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nomor_kartu_keluarga">
                Nomor Kartu Keluarga
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nomor_kartu_keluarga') is-invalid @enderror"
                name="nomor_kartu_keluarga" type="text" id="nomor_kartu_keluarga"
                value="{{ old('nomor_kartu_keluarga') }}">
            @error('nomor_kartu_keluarga')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="kewarganegaraan">
                Kewarganegaraan
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('kewarganegaraan') is-invalid @enderror" id="kewarganegaraan">
                <select name="kewarganegaraan">
                    <option value=""></option>
                    @foreach (get_negara_all() as $negara)
                        <option value="{{ $negara->kode }}"
                            {{ old('kewarganegaraan') == $negara->kode ? 'selected' : '' }}>
                            {{ $negara->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('kewarganegaraan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>

<hr>
<h5 class="mb-3">Data Perkawinan</h5>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik_ayah_dari_suami">
                NIK Ayah dari Suami
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik_ayah_dari_suami') is-invalid @enderror"
                name="nik_ayah_dari_suami" type="text" id="nik_ayah_dari_suami"
                value="{{ old('nik_ayah_dari_suami') }}">
            @error('nik_ayah_dari_suami')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_ayah_dari_suami">
                Nama Ayah dari Suami
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_ayah_dari_suami') is-invalid @enderror"
                name="nama_ayah_dari_suami" type="text" id="nama_ayah_dari_suami"
                value="{{ old('nama_ayah_dari_suami') }}">
            @error('nama_ayah_dari_suami')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik_ibu_dari_suami">
                NIK Ibu dari Suami
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik_ibu_dari_suami') is-invalid @enderror"
                name="nik_ibu_dari_suami" type="text" id="nik_ibu_dari_suami"
                value="{{ old('nik_ibu_dari_suami') }}">
            @error('nik_ibu_dari_suami')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_ibu_dari_suami">
                Nama Ibu dari Suami
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_ibu_dari_suami') is-invalid @enderror"
                name="nama_ibu_dari_suami" type="text" id="nama_ibu_dari_suami"
                value="{{ old('nama_ibu_dari_suami') }}">
            @error('nama_ibu_dari_suami')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik_ayah_dari_istri">
                NIK Ayah dari Istri
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik_ayah_dari_istri') is-invalid @enderror"
                name="nik_ayah_dari_istri" type="text" id="nik_ayah_dari_istri"
                value="{{ old('nik_ayah_dari_istri') }}">
            @error('nik_ayah_dari_istri')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_ayah_dari_istri">
                Nama Ayah dari Istri
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_ayah_dari_istri') is-invalid @enderror"
                name="nama_ayah_dari_istri" type="text" id="nama_ayah_dari_istri"
                value="{{ old('nama_ayah_dari_istri') }}">
            @error('nama_ayah_dari_istri')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik_ibu_dari_istri">
                NIK Ibu dari Istri
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik_ibu_dari_istri') is-invalid @enderror"
                name="nik_ibu_dari_istri" type="text" id="nik_ibu_dari_istri"
                value="{{ old('nik_ibu_dari_istri') }}">
            @error('nik_ibu_dari_istri')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_ibu_dari_istri">
                Nama Ibu dari Istri
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_ibu_dari_istri') is-invalid @enderror"
                name="nama_ibu_dari_istri" type="text" id="nama_ibu_dari_istri"
                value="{{ old('nama_ibu_dari_istri') }}">
            @error('nama_ibu_dari_istri')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="status_perkawinan_sebelum_kawin">
                Status Perkawinan Sebelum Kawin
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('status_perkawinan_sebelum_kawin') is-invalid @enderror"
                id="status_perkawinan_sebelum_kawin">
                <select name="status_perkawinan_sebelum_kawin">
                    <option value=""></option>
                    @foreach (get_status_perkawinan_sebelum_kawin_all() as $status_perkawinan_sebelum_kawin)
                        <option value="{{ $status_perkawinan_sebelum_kawin->kode }}"
                            {{ old('status_perkawinan_sebelum_kawin') == $status_perkawinan_sebelum_kawin->kode ? 'selected' : '' }}>
                            {{ $status_perkawinan_sebelum_kawin->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('status_perkawinan_sebelum_kawin')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="perkawinan_yang_ke">
                Perkawinan Yang ke-
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('perkawinan_yang_ke') is-invalid @enderror"
                name="perkawinan_yang_ke" type="text" id="perkawinan_yang_ke"
                value="{{ old('perkawinan_yang_ke') }}">
            @error('perkawinan_yang_ke')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="istri_yang_ke">
                Istri Yang ke- (bagi yang poligami)
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('istri_yang_ke') is-invalid @enderror" name="istri_yang_ke"
                type="text" id="istri_yang_ke" value="{{ old('istri_yang_ke') }}">
            @error('istri_yang_ke')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="tgl_pemberkatan_perkawinan">
                Tanggal Pemberkatan Perkawinan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_pemberkatan_perkawinan') is-invalid @enderror"
                name="tgl_pemberkatan_perkawinan" type="date" id="tgl_pemberkatan_perkawinan"
                value="{{ old('tgl_pemberkatan_perkawinan') }}">
            @error('tgl_pemberkatan_perkawinan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="tgl_melapor">
                Tanggal Melapor
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_melapor') is-invalid @enderror" name="tgl_melapor"
                type="date" id="tgl_melapor" value="{{ old('tgl_melapor') }}">
            @error('tgl_melapor')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="jam_pelaporan">
                Jam Pelaporan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('jam_pelaporan') is-invalid @enderror" name="jam_pelaporan"
                type="time" id="jam_pelaporan" value="{{ old('jam_pelaporan') }}">
            @error('jam_pelaporan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="agama_kepercayaan">
                Agama
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('agama_kepercayaan') is-invalid @enderror"
                id="agama_kepercayaan">
                <select name="agama_kepercayaan">
                    <option value=""></option>
                    @foreach (get_agama_all() as $agama_kepercayaan)
                        <option value="{{ $agama_kepercayaan->kode }}"
                            {{ old('agama_kepercayaan') == $agama_kepercayaan->kode ? 'selected' : '' }}>
                            {{ $agama_kepercayaan->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('agama_kepercayaan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="nama_organisasi_kepercayaan">
                Nama Organisasi Kepercayaan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_organisasi_kepercayaan') is-invalid @enderror"
                name="nama_organisasi_kepercayaan" type="text" id="nama_organisasi_kepercayaan"
                value="{{ old('nama_organisasi_kepercayaan') }}">
            @error('nama_organisasi_kepercayaan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="nama_pengadilan">
                Nama Pengadilan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_pengadilan') is-invalid @enderror" name="nama_pengadilan"
                type="text" id="nama_pengadilan" value="{{ old('nama_pengadilan') }}">
            @error('nama_pengadilan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="no_penetapan_pengadilan">
                Nomor Penetapan Pengadilan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('no_penetapan_pengadilan') is-invalid @enderror"
                name="no_penetapan_pengadilan" type="text" id="no_penetapan_pengadilan"
                value="{{ old('no_penetapan_pengadilan') }}">
            @error('no_penetapan_pengadilan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="tgl_penepatan_pengadilan">
                Tanggal Penetapan Pengadilan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_penepatan_pengadilan') is-invalid @enderror"
                name="tgl_penepatan_pengadilan" type="date" id="tgl_penepatan_pengadilan"
                value="{{ old('tgl_penepatan_pengadilan') }}">
            @error('tgl_penepatan_pengadilan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="nama_pemuka_agama">
                Nama Pemuka Agama
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_pemuka_agama') is-invalid @enderror"
                name="nama_pemuka_agama" type="text" id="nama_pemuka_agama" value="{{ old('nama_pemuka_agama') }}">
            @error('nama_pemuka_agama')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="no_surat_izin_dari_perwakilan">
                Nomor Surat Izin dari Perwakilan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('no_surat_izin_dari_perwakilan') is-invalid @enderror"
                name="no_surat_izin_dari_perwakilan" type="text" id="no_surat_izin_dari_perwakilan"
                value="{{ old('no_surat_izin_dari_perwakilan') }}">
            @error('no_surat_izin_dari_perwakilan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="no_passport">
                Nomor Passport
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('no_passport') is-invalid @enderror" name="no_passport"
                type="text" id="no_passport" value="{{ old('no_passport') }}">
            @error('no_passport')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="perjanjian_perkawinan">
                Perjanjian dibuat oleh Notaris
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('perjanjian_perkawinan') is-invalid @enderror"
                name="perjanjian_perkawinan" type="text" id="perjanjian_perkawinan"
                value="{{ old('perjanjian_perkawinan') }}">
            @error('perjanjian_perkawinan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="no_akta_notaris">
                Nomor Akta Notaris
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('no_akta_notaris') is-invalid @enderror" name="no_akta_notaris"
                type="text" id="no_akta_notaris" value="{{ old('no_akta_notaris') }}">
            @error('no_akta_notaris')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="tgl_akta_notaris">
                Tanggal Akta Notaris
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_akta_notaris') is-invalid @enderror"
                name="tgl_akta_notaris" type="date" id="tgl_akta_notaris" value="{{ old('tgl_akta_notaris') }}">
            @error('tgl_akta_notaris')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="jumlah_anak">
                Jumlah Anak
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('jumlah_anak') is-invalid @enderror" name="jumlah_anak"
                type="text" id="jumlah_anak" value="{{ old('jumlah_anak') }}">
            @error('jumlah_anak')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>
