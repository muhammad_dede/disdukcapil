{{-- data pelapor --}}
<h5 class="mb-3">Data Pelapor</h5>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_pelapor">
                Nama Lengkap Pelapor
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_pelapor') is-invalid @enderror" name="nama_pelapor"
                type="text" id="nama_pelapor" value="{{ old('nama_pelapor') }}">
            @error('nama_pelapor')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik">
                NIK
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik') is-invalid @enderror" name="nik" type="text" id="nik"
                value="{{ old('nik') }}">
            @error('nik')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nomor_kartu_keluarga">
                Nomor Kartu Keluarga
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nomor_kartu_keluarga') is-invalid @enderror"
                name="nomor_kartu_keluarga" type="text" id="nomor_kartu_keluarga"
                value="{{ old('nomor_kartu_keluarga') }}">
            @error('nomor_kartu_keluarga')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="kewarganegaraan">
                Kewarganegaraan
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('kewarganegaraan') is-invalid @enderror" id="kewarganegaraan">
                <select name="kewarganegaraan">
                    <option value=""></option>
                    @foreach (get_negara_all() as $negara)
                        <option value="{{ $negara->kode }}"
                            {{ old('kewarganegaraan') == $negara->kode ? 'selected' : '' }}>
                            {{ $negara->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('kewarganegaraan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>

<hr>
<h5 class="mb-3">Data Perkawinan</h5>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="tgl_perkawinan">
                Tanggal Perkawinan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_perkawinan') is-invalid @enderror" name="tgl_perkawinan"
                type="date" id="tgl_perkawinan" value="{{ old('tgl_perkawinan') }}">
            @error('tgl_perkawinan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="no_akta_perkawinan">
                Nomor Akta Perkawinan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('no_akta_perkawinan') is-invalid @enderror"
                name="no_akta_perkawinan" type="text" id="no_akta_perkawinan"
                value="{{ old('no_akta_perkawinan') }}">
            @error('no_akta_perkawinan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="tgl_akta_perkawinan">
                Tanggal Akta Perkawinan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_akta_perkawinan') is-invalid @enderror"
                name="tgl_akta_perkawinan" type="date" id="tgl_akta_perkawinan"
                value="{{ old('tgl_akta_perkawinan') }}">
            @error('tgl_akta_perkawinan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_pengadilan_pembatalan">
                Nama Pengadilan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_pengadilan_pembatalan') is-invalid @enderror"
                name="nama_pengadilan_pembatalan" type="text" id="nama_pengadilan_pembatalan"
                value="{{ old('nama_pengadilan_pembatalan') }}">
            @error('nama_pengadilan_pembatalan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="no_putusan_pengadilan">
                Nomor Putusan Pengadilan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('no_putusan_pengadilan') is-invalid @enderror"
                name="no_putusan_pengadilan" type="text" id="no_putusan_pengadilan"
                value="{{ old('no_putusan_pengadilan') }}">
            @error('no_putusan_pengadilan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="tgl_putusan_pengadilan">
                Tanggal Putusan Pengadilan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_putusan_pengadilan') is-invalid @enderror"
                name="tgl_putusan_pengadilan" type="date" id="tgl_putusan_pengadilan"
                value="{{ old('tgl_putusan_pengadilan') }}">
            @error('tgl_putusan_pengadilan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="tgl_pelaporan_perkawinan_luar_negeri">
                Tanggal Pelaporan Perkawinan diluar Negeri
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_pelaporan_perkawinan_luar_negeri') is-invalid @enderror"
                name="tgl_pelaporan_perkawinan_luar_negeri" type="date" id="tgl_pelaporan_perkawinan_luar_negeri"
                value="{{ old('tgl_pelaporan_perkawinan_luar_negeri') }}">
            @error('tgl_pelaporan_perkawinan_luar_negeri')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>
