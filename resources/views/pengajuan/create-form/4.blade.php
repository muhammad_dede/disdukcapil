{{-- data pelapor --}}
<h5 class="mb-3">Data Pelapor</h5>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_pelapor">
                Nama Lengkap Pelapor
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_pelapor') is-invalid @enderror" name="nama_pelapor"
                type="text" id="nama_pelapor" value="{{ old('nama_pelapor') }}">
            @error('nama_pelapor')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik">
                NIK
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik') is-invalid @enderror" name="nik" type="text" id="nik"
                value="{{ old('nik') }}">
            @error('nik')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nomor_kartu_keluarga">
                Nomor Kartu Keluarga
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nomor_kartu_keluarga') is-invalid @enderror"
                name="nomor_kartu_keluarga" type="text" id="nomor_kartu_keluarga"
                value="{{ old('nomor_kartu_keluarga') }}">
            @error('nomor_kartu_keluarga')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="kewarganegaraan">
                Kewarganegaraan
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('kewarganegaraan') is-invalid @enderror" id="kewarganegaraan">
                <select name="kewarganegaraan">
                    <option value=""></option>
                    @foreach (get_negara_all() as $negara)
                        <option value="{{ $negara->kode }}"
                            {{ old('kewarganegaraan') == $negara->kode ? 'selected' : '' }}>
                            {{ $negara->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('kewarganegaraan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>

<hr>
<h5 class="mb-3">Data Lahir Mati</h5>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="lamanya_dalam_kandungan">
                Lamanya Dalam Kandungan
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('lamanya_dalam_kandungan') is-invalid @enderror"
                name="lamanya_dalam_kandungan" type="text" id="lamanya_dalam_kandungan"
                value="{{ old('lamanya_dalam_kandungan') }}">
            @error('lamanya_dalam_kandungan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="jenis_kelamin">
                Jenis Kelamin
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('jenis_kelamin') is-invalid @enderror" id="jenis_kelamin">
                <select name="jenis_kelamin">
                    <option value=""></option>
                    @foreach (get_jenis_kelamin_all() as $jenis_kelamin)
                        <option value="{{ $jenis_kelamin->kode }}"
                            {{ old('jenis_kelamin') == $jenis_kelamin->kode ? 'selected' : '' }}>
                            {{ $jenis_kelamin->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('jenis_kelamin')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="tgl_lahir_mati">
                Tanggal Lahir Mati
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_lahir_mati') is-invalid @enderror" name="tgl_lahir_mati"
                type="date" id="tgl_lahir_mati" value="{{ old('tgl_lahir_mati') }}">
            @error('tgl_lahir_mati')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="jenis_kelahiran">
                Jenis Kelahiran
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('jenis_kelahiran') is-invalid @enderror" id="jenis_kelahiran">
                <select name="jenis_kelahiran">
                    <option value=""></option>
                    @foreach (get_jenis_kelahiran_all() as $jenis_kelahiran)
                        <option value="{{ $jenis_kelahiran->kode }}"
                            {{ old('jenis_kelahiran') == $jenis_kelahiran->kode ? 'selected' : '' }}>
                            {{ $jenis_kelahiran->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('jenis_kelahiran')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="anak_ke">
                Anak ke
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('anak_ke') is-invalid @enderror" name="anak_ke" type="text"
                id="anak_ke" value="{{ old('anak_ke') }}">
            @error('anak_ke')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="tempat_dilahirkan">
                Tempat dilahirkan
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('tempat_dilahirkan') is-invalid @enderror" id="tempat_dilahirkan">
                <select name="tempat_dilahirkan">
                    <option value=""></option>
                    @foreach (get_tempat_dilahirkan_all() as $tempat_dilahirkan)
                        <option value="{{ $tempat_dilahirkan->kode }}"
                            {{ old('tempat_dilahirkan') == $tempat_dilahirkan->kode ? 'selected' : '' }}>
                            {{ $tempat_dilahirkan->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('tempat_dilahirkan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="penolong_kelahiran">
                Penolong Kelahiran
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('penolong_kelahiran') is-invalid @enderror"
                id="penolong_kelahiran">
                <select name="penolong_kelahiran">
                    <option value=""></option>
                    @foreach (get_tenaga_ahli_all() as $penolong_kelahiran)
                        <option value="{{ $penolong_kelahiran->kode }}"
                            {{ old('penolong_kelahiran') == $penolong_kelahiran->kode ? 'selected' : '' }}>
                            {{ $penolong_kelahiran->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('penolong_kelahiran')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-6">
            <label class="control-label" for="sebab_lahir_mati">
                Sebab Lahir Mati
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('sebab_lahir_mati') is-invalid @enderror" name="sebab_lahir_mati"
                type="text" id="sebab_lahir_mati" value="{{ old('sebab_lahir_mati') }}">
            @error('sebab_lahir_mati')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="yang_menentukan">
                Yang Menentukan
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('yang_menentukan') is-invalid @enderror" id="yang_menentukan">
                <select name="yang_menentukan">
                    <option value=""></option>
                    @foreach (get_yang_menentukan_mati_all() as $yang_menentukan)
                        <option value="{{ $yang_menentukan->kode }}"
                            {{ old('yang_menentukan') == $yang_menentukan->kode ? 'selected' : '' }}>
                            {{ $yang_menentukan->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('yang_menentukan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="tempat_kelahiran">
                Tempat Kelahiran
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tempat_kelahiran') is-invalid @enderror" name="tempat_kelahiran"
                type="text" id="tempat_kelahiran" value="{{ old('tempat_kelahiran') }}">
            @error('tempat_kelahiran')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>

{{-- data ortu --}}
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_ayah">
                Nama Ayah
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_ayah') is-invalid @enderror" name="nama_ayah" type="text"
                id="nama_ayah" value="{{ old('nama_ayah') }}">
            @error('nama_ayah')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik_ayah">
                NIK Ayah
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik_ayah') is-invalid @enderror" name="nik_ayah" type="text"
                id="nik_ayah" value="{{ old('nik_ayah') }}">
            @error('nik_ayah')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="tempat_lahir_ayah">
                Tempat Lahir Ayah
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tempat_lahir_ayah') is-invalid @enderror"
                name="tempat_lahir_ayah" type="text" id="tempat_lahir_ayah" value="{{ old('tempat_lahir_ayah') }}">
            @error('tempat_lahir_ayah')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="tgl_lahir_ayah">
                Tanggal Lahir Ayah
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_lahir_ayah') is-invalid @enderror" name="tgl_lahir_ayah"
                type="date" id="tgl_lahir_ayah" value="{{ old('tgl_lahir_ayah') }}">
            @error('tgl_lahir_ayah')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="kewarganegaraan_ayah">
                Kewarganegaraan Ayah
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('kewarganegaraan_ayah') is-invalid @enderror"
                id="kewarganegaraan_ayah">
                <select name="kewarganegaraan_ayah">
                    <option value=""></option>
                    @foreach (get_negara_all() as $negara)
                        <option value="{{ $negara->kode }}"
                            {{ old('kewarganegaraan_ayah') == $negara->kode ? 'selected' : '' }}>
                            {{ $negara->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('kewarganegaraan_ayah')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_ibu">
                Nama Ibu
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_ibu') is-invalid @enderror" name="nama_ibu" type="text"
                id="nama_ibu" value="{{ old('nama_ibu') }}">
            @error('nama_ibu')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik_ibu">
                NIK Ibu
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik_ibu') is-invalid @enderror" name="nik_ibu" type="text"
                id="nik_ibu" value="{{ old('nik_ibu') }}">
            @error('nik_ibu')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="tempat_lahir_ibu">
                Tempat Lahir Ibu
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tempat_lahir_ibu') is-invalid @enderror"
                name="tempat_lahir_ibu" type="text" id="tempat_lahir_ibu" value="{{ old('tempat_lahir_ibu') }}">
            @error('tempat_lahir_ibu')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="tgl_lahir_ibu">
                Tanggal Lahir Ibu
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('tgl_lahir_ibu') is-invalid @enderror" name="tgl_lahir_ibu"
                type="date" id="tgl_lahir_ibu" value="{{ old('tgl_lahir_ibu') }}">
            @error('tgl_lahir_ibu')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="kewarganegaraan_ibu">
                Kewarganegaraan Ibu
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('kewarganegaraan_ibu') is-invalid @enderror"
                id="kewarganegaraan_ibu">
                <select name="kewarganegaraan_ibu">
                    <option value=""></option>
                    @foreach (get_negara_all() as $negara)
                        <option value="{{ $negara->kode }}"
                            {{ old('kewarganegaraan_ibu') == $negara->kode ? 'selected' : '' }}>
                            {{ $negara->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('kewarganegaraan_ibu')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>


{{-- data saksi --}}
<hr>
<h5 class="mb-3">Data Saksi</h5>
<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_saksi_1">
                Nama Lengkap Saksi 1
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_saksi_1') is-invalid @enderror" name="nama_saksi_1"
                type="text" id="nama_saksi_1" value="{{ old('nama_saksi_1') }}">
            @error('nama_saksi_1')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik_saksi_1">
                NIK
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik_saksi_1') is-invalid @enderror" name="nik_saksi_1"
                type="text" id="nik_saksi_1" value="{{ old('nik_saksi_1') }}">
            @error('nik_saksi_1')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="no_kk_saksi_1">
                Nomor Kartu Keluarga
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('no_kk_saksi_1') is-invalid @enderror" name="no_kk_saksi_1"
                type="text" id="no_kk_saksi_1" value="{{ old('no_kk_saksi_1') }}">
            @error('no_kk_saksi_1')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="kewarganegaraan_saksi_1">
                Kewarganegaraan
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('kewarganegaraan_saksi_1') is-invalid @enderror"
                id="kewarganegaraan_saksi_1">
                <select name="kewarganegaraan_saksi_1">
                    <option value=""></option>
                    @foreach (get_negara_all() as $negara)
                        <option value="{{ $negara->kode }}"
                            {{ old('kewarganegaraan_saksi_1') == $negara->kode ? 'selected' : '' }}>
                            {{ $negara->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('kewarganegaraan_saksi_1')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_saksi_2">
                Nama Lengkap Saksi 2
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_saksi_2') is-invalid @enderror" name="nama_saksi_2"
                type="text" id="nama_saksi_2" value="{{ old('nama_saksi_2') }}">
            @error('nama_saksi_2')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik_saksi_2">
                NIK
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik_saksi_2') is-invalid @enderror" name="nik_saksi_2"
                type="text" id="nik_saksi_2" value="{{ old('nik_saksi_2') }}">
            @error('nik_saksi_2')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="no_kk_saksi_2">
                Nomor Kartu Keluarga
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('no_kk_saksi_2') is-invalid @enderror" name="no_kk_saksi_2"
                type="text" id="no_kk_saksi_2" value="{{ old('no_kk_saksi_2') }}">
            @error('no_kk_saksi_2')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="kewarganegaraan_saksi_2">
                Kewarganegaraan
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('kewarganegaraan_saksi_2') is-invalid @enderror"
                id="kewarganegaraan_saksi_2">
                <select name="kewarganegaraan_saksi_2">
                    <option value=""></option>
                    @foreach (get_negara_all() as $negara)
                        <option value="{{ $negara->kode }}"
                            {{ old('kewarganegaraan_saksi_2') == $negara->kode ? 'selected' : '' }}>
                            {{ $negara->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('kewarganegaraan_saksi_2')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>
