<div class="row">
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nama_lengkap">
                Nama Lengkap
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap"
                type="text" id="nama_lengkap" value="{{ old('nama_lengkap') }}">
            @error('nama_lengkap')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="nik">
                NIK
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('nik') is-invalid @enderror" name="nik" type="text" id="nik"
                value="{{ old('nik') }}">
            @error('nik')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group mb-3">
            <label class="control-label" for="alamat_rumah">
                Alamat Lengkap Rumah
                <sup class="text-danger">*</sup>
            </label>
            <input class="form-control input-md @error('alamat_rumah') is-invalid @enderror" name="alamat_rumah"
                type="text" id="alamat_rumah" value="{{ old('alamat_rumah') }}">
            @error('alamat_rumah')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <h5>Pendidikan Terakhir</h5>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="pendidikan_terakhir_semula">
                Semula
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('pendidikan_terakhir_semula') is-invalid @enderror"
                id="pendidikan_terakhir_semula">
                <select name="pendidikan_terakhir_semula">
                    <option value=""></option>
                    @foreach (get_pendidikan_all() as $pendidikan)
                        <option value="{{ $pendidikan->kode }}"
                            {{ old('pendidikan_terakhir_semula') == $pendidikan->kode ? 'selected' : '' }}>
                            {{ $pendidikan->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('pendidikan_terakhir_semula')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="pendidikan_terakhir_menjadi">
                Menjadi
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('pendidikan_terakhir_menjadi') is-invalid @enderror"
                id="pendidikan_terakhir_menjadi">
                <select name="pendidikan_terakhir_menjadi">
                    <option value=""></option>
                    @foreach (get_pendidikan_all() as $pendidikan)
                        <option value="{{ $pendidikan->kode }}"
                            {{ old('pendidikan_terakhir_menjadi') == $pendidikan->kode ? 'selected' : '' }}>
                            {{ $pendidikan->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('pendidikan_terakhir_menjadi')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="pendidikan_terakhir_dasar_perubahan">
                Dasar Perubahan
            </label>
            <input class="form-control input-md @error('pendidikan_terakhir_dasar_perubahan') is-invalid @enderror"
                name="pendidikan_terakhir_dasar_perubahan" type="text" id="pendidikan_terakhir_dasar_perubahan"
                value="{{ old('pendidikan_terakhir_dasar_perubahan') }}">
            @error('pendidikan_terakhir_dasar_perubahan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="pendidikan_terakhir_no">
                No
            </label>
            <input class="form-control input-md @error('pendidikan_terakhir_no') is-invalid @enderror"
                name="pendidikan_terakhir_no" type="text" id="pendidikan_terakhir_no"
                value="{{ old('pendidikan_terakhir_no') }}">
            @error('pendidikan_terakhir_no')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="pendidikan_terakhir_tgl">
                Tanggal
            </label>
            <input class="form-control input-md @error('pendidikan_terakhir_tgl') is-invalid @enderror"
                name="pendidikan_terakhir_tgl" type="date" id="pendidikan_terakhir_tgl"
                value="{{ old('pendidikan_terakhir_tgl') }}">
            @error('pendidikan_terakhir_tgl')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <h5>Pekerjaan</h5>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="pekerjaan_semula">
                Semula
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('pekerjaan_semula') is-invalid @enderror" id="pekerjaan_semula">
                <select name="pekerjaan_semula">
                    <option value=""></option>
                    @foreach (get_pekerjaan_all() as $pekerjaan)
                        <option value="{{ $pekerjaan->kode }}"
                            {{ old('pekerjaan_semula') == $pekerjaan->kode ? 'selected' : '' }}>
                            {{ $pekerjaan->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('pekerjaan_semula')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="pekerjaan_menjadi">
                Menjadi
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('pekerjaan_menjadi') is-invalid @enderror" id="pekerjaan_menjadi">
                <select name="pekerjaan_menjadi">
                    <option value=""></option>
                    @foreach (get_pekerjaan_all() as $pekerjaan)
                        <option value="{{ $pekerjaan->kode }}"
                            {{ old('pekerjaan_menjadi') == $pekerjaan->kode ? 'selected' : '' }}>
                            {{ $pekerjaan->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('pekerjaan_menjadi')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="pekerjaan_dasar_perubahan">
                Dasar Perubahan
            </label>
            <input class="form-control input-md @error('pekerjaan_dasar_perubahan') is-invalid @enderror"
                name="pekerjaan_dasar_perubahan" type="text" id="pekerjaan_dasar_perubahan"
                value="{{ old('pekerjaan_dasar_perubahan') }}">
            @error('pekerjaan_dasar_perubahan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="pekerjaan_no">
                No
            </label>
            <input class="form-control input-md @error('pekerjaan_no') is-invalid @enderror" name="pekerjaan_no"
                type="text" id="pekerjaan_no" value="{{ old('pekerjaan_no') }}">
            @error('pekerjaan_no')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="pekerjaan_terakhir_tgl">
                Tanggal
            </label>
            <input class="form-control input-md @error('pekerjaan_tgl') is-invalid @enderror" name="pekerjaan_tgl"
                type="date" id="pekerjaan_tgl" value="{{ old('pekerjaan_tgl') }}">
            @error('pekerjaan_tgl')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <h5>Agama</h5>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="agama_semula">
                Semula
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('agama_semula') is-invalid @enderror" id="agama_semula">
                <select name="agama_semula">
                    <option value=""></option>
                    @foreach (get_agama_all() as $agama)
                        <option value="{{ $agama->kode }}"
                            {{ old('agama_semula') == $agama->kode ? 'selected' : '' }}>
                            {{ $agama->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('agama_semula')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3 tg-inputwithicon">
            <label class="control-label" for="agama_menjadi">
                Menjadi
                <sup class="text-danger">*</sup>
            </label>
            <div class="tg-select form-control @error('agama_menjadi') is-invalid @enderror" id="agama_menjadi">
                <select name="agama_menjadi">
                    <option value=""></option>
                    @foreach (get_agama_all() as $agama)
                        <option value="{{ $agama->kode }}"
                            {{ old('agama_menjadi') == $agama->kode ? 'selected' : '' }}>
                            {{ $agama->nama }}
                        </option>
                    @endforeach
                </select>
            </div>
            @error('agama_menjadi')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="agama_dasar_perubahan">
                Dasar Perubahan
            </label>
            <input class="form-control input-md @error('agama_dasar_perubahan') is-invalid @enderror"
                name="agama_dasar_perubahan" type="text" id="agama_dasar_perubahan"
                value="{{ old('agama_dasar_perubahan') }}">
            @error('agama_dasar_perubahan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="agama_no">
                No
            </label>
            <input class="form-control input-md @error('agama_no') is-invalid @enderror" name="agama_no" type="text"
                id="agama_no" value="{{ old('agama_no') }}">
            @error('agama_no')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="agama_tgl">
                Tanggal
            </label>
            <input class="form-control input-md @error('agama_tgl') is-invalid @enderror" name="agama_tgl" type="date"
                id="agama_tgl" value="{{ old('agama_tgl') }}">
            @error('agama_tgl')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <h5>Perubahan Lainnya</h5>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="perubahan_lainnya_semula">
                Semula
            </label>
            <input class="form-control input-md @error('perubahan_lainnya_semula') is-invalid @enderror"
                name="perubahan_lainnya_semula" type="text" id="perubahan_lainnya_semula"
                value="{{ old('perubahan_lainnya_semula') }}">
            @error('perubahan_lainnya_semula')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="perubahan_lainnya_menjadi">
                Menjadi
            </label>
            <input class="form-control input-md @error('perubahan_lainnya_menjadi') is-invalid @enderror"
                name="perubahan_lainnya_menjadi" type="text" id="perubahan_lainnya_menjadi"
                value="{{ old('perubahan_lainnya_menjadi') }}">
            @error('perubahan_lainnya_menjadi')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group mb-3">
            <label class="control-label" for="perubahan_lainnya_dasar_perubahan">
                Dasar Perubahan
            </label>
            <input class="form-control input-md @error('perubahan_lainnya_dasar_perubahan') is-invalid @enderror"
                name="perubahan_lainnya_dasar_perubahan" type="text" id="perubahan_lainnya_dasar_perubahan"
                value="{{ old('perubahan_lainnya_dasar_perubahan') }}">
            @error('perubahan_lainnya_dasar_perubahan')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="perubahan_lainnya_no">
                No
            </label>
            <input class="form-control input-md @error('perubahan_lainnya_no') is-invalid @enderror"
                name="perubahan_lainnya_no" type="text" id="perubahan_lainnya_no"
                value="{{ old('perubahan_lainnya_no') }}">
            @error('perubahan_lainnya_no')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group mb-3">
            <label class="control-label" for="perubahan_lainnya_tgl">
                Tanggal
            </label>
            <input class="form-control input-md @error('perubahan_lainnya_tgl') is-invalid @enderror"
                name="perubahan_lainnya_tgl" type="date" id="perubahan_lainnya_tgl"
                value="{{ old('perubahan_lainnya_tgl') }}">
            @error('perubahan_lainnya_tgl')
                <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
    </div>
</div>
