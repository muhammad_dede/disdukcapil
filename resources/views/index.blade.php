@extends('layouts.app')

@section('content')
    <section id="hero">
        <div id="hero-area">
            <div class="overlay"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-12 col-lg-9 col-xs-12 text-center">
                        <div class="contents">
                            <h1 class="head-title ">
                                SMART DUKCAPIL KOTA CILEGON
                            </h1>
                            <p>
                                SALAH SATU INOVASI LAYANAN PENGGANTI TATAP MUKA, SEHINGGA PEMOHON ATAU WARGA DAPAT MENGURUS
                                DOKUMEN DARI MANAPUN DAN KAPANPUN
                            </p>
                            <a href="{{ route('layanan') }}" class="btn btn-common mt-3 px-3">LAYANAN KAMI</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="works section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="section-title">Cara Mengajukannya?</h3>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="works-item">
                        <div class="icon-box">
                            <i class="lni-users"></i>
                        </div>
                        <p><small>Buat Akun</small></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="works-item">
                        <div class="icon-box">
                            <i class="lni-bookmark-alt"></i>
                        </div>
                        <p><small>Buat Pengajuan Baru</small></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="works-item">
                        <div class="icon-box">
                            <i class="lni-timer"></i>
                        </div>
                        <p><small>Pengajuan Diproses</small></p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-12">
                    <div class="works-item">
                        <div class="icon-box">
                            <i class="lni-thumbs-up"></i>
                        </div>
                        <p><small>Selesai</small></p>
                    </div>
                </div>
                <hr class="works-line">
            </div>
        </div>
    </section>

    <section id="pricing-table" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="section-title">Jenis Layanan</h2>
                </div>
                @php
                    $count = 0;
                @endphp
                @forelse (get_layanan() as $layanan)
                    <?php if ($count == 3) {
                        break;
                    } ?>
                    <div class="col-lg-4 col-md-6 col-xs-12">
                        <div class="table">
                            <div class="icon">
                                <img src="{{ asset('') }}assets/img/category/img-3.png" alt="">
                            </div>
                            <div class="title">
                                <h3 class="text-muted">{{ $layanan->layanan }}</h3>
                            </div>
                            <ul class="description">
                                <li>{{ $layanan->keterangan }}</li>
                            </ul>
                            <a href="{{ route('layanan.detail', $layanan->url) }}" class="btn btn-common">Lihat Detail</a>
                        </div>
                    </div>
                    <?php $count++; ?>
                @empty
                    <strong class="text-center">Tidak ada layanan</strong>
                @endforelse
            </div>
            <div class="row mt-4">
                <div class="col-12">
                    <a href="{{ route('layanan') }}" class="btn btn-common">Semua Layanan Kami</a>
                </div>
            </div>
        </div>
    </section>

    <section class="counter-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 work-counter-widget">
                    <div class="counter">
                        <div class="icon"><i class="lni-users"></i></div>
                        <h2 class="counterUp">{{ get_counter()['warga'] }}</h2>
                        <p>Pengguna</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 work-counter-widget">
                    <div class="counter">
                        <div class="icon"><i class="lni-files"></i></div>
                        <h2 class="counterUp">{{ get_counter()['pengajuan'] }}</h2>
                        <p>Total Pengajuan</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 work-counter-widget">
                    <div class="counter">
                        <div class="icon"><i class="lni-timer"></i></div>
                        <h2 class="counterUp">{{ get_counter()['proses'] }}</h2>
                        <p>Dalam Proses</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 work-counter-widget">
                    <div class="counter">
                        <div class="icon"><i class="lni-thumbs-up"></i></div>
                        <h2 class="counterUp">{{ get_counter()['selesai'] }}</h2>
                        <p>Selesai</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
