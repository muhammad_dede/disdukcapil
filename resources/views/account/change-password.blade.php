@extends('layouts.app')

@section('title', 'Ubah Password')

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Ubah Password</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Ubah Password</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="section-padding">
        <div class="container">
            <div class="row">
                @if (Session::has('success'))
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ Session::get('success') }}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
                <div class="col-sm-12 col-md-4 col-lg-3 page-sidebar">
                    @include('layouts.aside')
                </div>
                <div class="col-sm-12 col-md-8 col-lg-9">
                    <div class="row page-content">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="inner-box">
                                <div class="dashboard-box">
                                    <h2 class="dashbord-title">Ubah Password</h2>
                                </div>
                                <div class="dashboard-wrapper">
                                    <form action="{{ route('user.account.update-password') }}" method="POST"
                                        enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group mb-3">
                                                    <label class="control-label" for="current_password">Password Saat
                                                        Ini</label>
                                                    <input
                                                        class="form-control input-md @error('current_password') is-invalid @enderror"
                                                        name="current_password" type="password" id="current_password">
                                                    @error('current_password')
                                                        <small class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group mb-3">
                                                    <label class="control-label" for="password">Password Yang
                                                        Baru</label>
                                                    <input
                                                        class="form-control input-md @error('password') is-invalid @enderror"
                                                        name="password" type="password" id="password">
                                                    @error('password')
                                                        <small class="text-danger">{{ $message }}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group mb-3">
                                                    <label class="control-label" for="password_confirmation">
                                                        Konfirmasi Password
                                                    </label>
                                                    <input class="form-control input-md" name="password_confirmation"
                                                        type="password" id="password_confirmation">
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-common btn-block mb-4 mt-3" type="submit">Simpan</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
