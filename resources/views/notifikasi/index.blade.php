@extends('layouts.app')

@section('title', 'Notifikasi')

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Notifikasi</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Notifikasi</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="content" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-3 page-sidebar">
                    @include('layouts.aside')
                </div>
                <div class="col-sm-12 col-md-8 col-lg-9">
                    <div class="page-content">
                        <div class="inner-box">
                            <div class="dashboard-box">
                                <h2 class="dashbord-title">Notifikasi</h2>
                            </div>
                            <div class="dashboard-wrapper table-responsive">
                                <a href="{{ route('user.notifikasi.read-all') }}">Tandai sebagai sudah dibaca</a>
                                <table class="table table-hover mt-3">
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('my-js')
    <script src="{{ asset('') }}scripts/notifikasi/index.js"></script>
@endpush
