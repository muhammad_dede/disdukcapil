@extends('layouts.app')

@section('title', 'Daftar')

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Daftar</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Daftar</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="register section-padding">
        <div class="container">
            <div class="row page-content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="inner-box">
                        <div class="tg-contactdetail">
                            <div class="dashboard-box">
                                <h2 class="dashbord-title">Pendaftaran Akun Baru</h2>
                            </div>
                            <div class="dashboard-wrapper">
                                <form action="{{ route('register') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="alert alert-warning">
                                        <ol>
                                            <li>
                                                <small>Isikan form pendaftaran user baru dengan data yang sesuai</small>
                                            </li>
                                            <li>
                                                <small>Pendaftaran hanya berlaku untuk penduduk atau warga Kota
                                                    Cilegon</small>
                                            </li>
                                        </ol>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group mb-3">
                                                <label class="control-label" for="nik">Nomor Induk Kependudukan
                                                    (NIK)</label>
                                                <input class="form-control input-md @error('nik') is-invalid @enderror"
                                                    name="nik" type="text" id="nik" value="{{ old('nik') }}">
                                                @error('nik')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3">
                                                <label class="control-label" for="nama_lengkap">Nama Lengkap</label>
                                                <input
                                                    class="form-control input-md @error('nama_lengkap') is-invalid @enderror"
                                                    name="nama_lengkap" type="text" id="nama_lengkap"
                                                    value="{{ old('nama_lengkap') }}">
                                                @error('nama_lengkap')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3">
                                                <label class="control-label" for="tempat_lahir">Tempat Lahir</label>
                                                <input
                                                    class="form-control input-md @error('tempat_lahir') is-invalid @enderror"
                                                    name="tempat_lahir" type="text" id="tempat_lahir"
                                                    value="{{ old('tempat_lahir') }}">
                                                @error('tempat_lahir')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3">
                                                <label class="control-label" for="tgl_lahir">Tanggal Lahir</label>
                                                <input
                                                    class="form-control input-md @error('tgl_lahir') is-invalid @enderror"
                                                    name="tgl_lahir" type="date" id="tgl_lahir"
                                                    value="{{ old('tgl_lahir') }}">
                                                @error('tgl_lahir')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3 tg-inputwithicon">
                                                <label class="control-label" for="jenis_kelamin">Jenis Kelamin</label>
                                                <div class="tg-select form-control @error('jenis_kelamin') is-invalid @enderror"
                                                    id="jenis_kelamin">
                                                    <select name="jenis_kelamin">
                                                        <option value=""></option>
                                                        @foreach (get_jenis_kelamin() as $jk)
                                                            <option value="{{ $jk->kode }}"
                                                                {{ old('jenis_kelamin') == $jk->kode ? 'selected' : '' }}>
                                                                {{ $jk->nama }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @error('jenis_kelamin')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3">
                                                <label class="control-label" for="nama_ibu">Nama Ibu</label>
                                                <input class="form-control input-md @error('nama_ibu') is-invalid @enderror"
                                                    name="nama_ibu" type="text" id="nama_ibu"
                                                    value="{{ old('nama_ibu') }}">
                                                @error('nama_ibu')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3 tg-inputwithicon">
                                                <label class="control-label" for="gol_darah">Golongan Darah</label>
                                                <div class="tg-select form-control @error('gol_darah') is-invalid @enderror"
                                                    id="gol_darah">
                                                    <select name="gol_darah">
                                                        <option value=""></option>
                                                        @foreach (get_golongan_darah() as $goldar)
                                                            <option value="{{ $goldar->kode }}"
                                                                {{ old('gol_darah') == $goldar->kode ? 'selected' : '' }}>
                                                                {{ $goldar->nama }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @error('gol_darah')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3 tg-inputwithicon">
                                                <label class="control-label" for="kewarganegaraan">Kewarganegaraan</label>
                                                <div class="tg-select form-control @error('kewarganegaraan') is-invalid @enderror"
                                                    id="kewarganegaraan">
                                                    <select name="kewarganegaraan">
                                                        <option value="ID" selected>INDONESIA</option>
                                                    </select>
                                                </div>
                                                @error('kewarganegaraan')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3">
                                                <label class="control-label" for="alamat">Alamat</label>
                                                <input class="form-control input-md @error('alamat') is-invalid @enderror"
                                                    name="alamat" type="text" id="alamat" value="{{ old('alamat') }}">
                                                @error('alamat')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group mb-3 tg-inputwithicon">
                                                <label class="control-label" for="rt">RT</label>
                                                <div class="tg-select form-control @error('rt') is-invalid @enderror"
                                                    id="rt">
                                                    <select name="rt">
                                                        <option value=""></option>
                                                        @foreach (get_rt() as $rt)
                                                            <option value="{{ $rt['rt'] }}"
                                                                {{ old('rt') == $rt['rt'] ? 'selected' : '' }}>
                                                                {{ $rt['rt'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @error('rt')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group mb-3 tg-inputwithicon">
                                                <label class="control-label" for="rw">RW</label>
                                                <div class="tg-select form-control @error('rw') is-invalid @enderror"
                                                    id="rw">
                                                    <select name="rw">
                                                        <option value=""></option>
                                                        @foreach (get_rw() as $rw)
                                                            <option value="{{ $rw['rw'] }}"
                                                                {{ old('rw') == $rw['rw'] ? 'selected' : '' }}>
                                                                {{ $rw['rw'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @error('rw')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3 tg-inputwithicon">
                                                <label class="control-label" for="kelurahan">Kelurahan</label>
                                                <div class="tg-select form-control @error('kelurahan') is-invalid @enderror"
                                                    id="kelurahan">
                                                    <select name="kelurahan">
                                                        <option value=""></option>
                                                        @foreach (get_kelurahan_cilegon() as $kelurahan)
                                                            <option value="{{ $kelurahan['kode'] }}"
                                                                {{ old('kelurahan') == $kelurahan['kode'] ? 'selected' : '' }}>
                                                                {{ $kelurahan['nama'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @error('kelurahan')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3 tg-inputwithicon">
                                                <label class="control-label" for="kecamatan">Kecamatan</label>
                                                <div class="tg-select form-control @error('kecamatan') is-invalid @enderror"
                                                    id="kecamatan">
                                                    <select name="kecamatan">
                                                        <option value=""></option>
                                                        @foreach (get_kecamatan_cilegon() as $kecamatan)
                                                            <option value="{{ $kecamatan['kode'] }}"
                                                                {{ old('kecamatan') == $kecamatan['kode'] ? 'selected' : '' }}>
                                                                {{ $kecamatan['nama'] }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                @error('kecamatan')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3 tg-inputwithicon">
                                                <label class="control-label" for="kota">Kota</label>
                                                <div class="tg-select form-control @error('kota') is-invalid @enderror"
                                                    id="kota">
                                                    <select name="kota">
                                                        <option value="3672" selected>KOTA CILEGON</option>
                                                    </select>
                                                </div>
                                                @error('kota')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3 tg-inputwithicon">
                                                <label class="control-label" for="provinsi">Provinsi</label>
                                                <div class="tg-select form-control @error('provinsi') is-invalid @enderror"
                                                    id="provinsi">
                                                    <select name="provinsi">
                                                        <option value="36" selected>BANTEN</option>
                                                    </select>
                                                </div>
                                                @error('provinsi')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3">
                                                <label class="control-label" for="kode_pos">Kode POS</label>
                                                <input class="form-control input-md @error('kode_pos') is-invalid @enderror"
                                                    name="kode_pos" type="text" id="kode_pos"
                                                    value="{{ old('kode_pos') }}">
                                                @error('kode_pos')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3">
                                                <label class="control-label" for="no_whatsapp">Nomor Whatsapp</label>
                                                <input
                                                    class="form-control input-md @error('no_whatsapp') is-invalid @enderror"
                                                    name="no_whatsapp" type="text" id="no_whatsapp"
                                                    value="{{ old('no_whatsapp') }}">
                                                @error('no_whatsapp')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group mb-3">
                                                <label class="control-label" for="email">Email</label>
                                                <input class="form-control input-md @error('email') is-invalid @enderror"
                                                    name="email" type="text" id="email" value="{{ old('email') }}">
                                                @error('email')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3">
                                                <label class="control-label" for="password">Password</label>
                                                <input
                                                    class="form-control input-md @error('password') is-invalid @enderror"
                                                    name="password" type="password" id="password">
                                                @error('password')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3">
                                                <label class="control-label" for="password_confirmation">Ulangi
                                                    Password</label>
                                                <input
                                                    class="form-control input-md @error('password_confirmation') is-invalid @enderror"
                                                    name="password_confirmation" type="password" id="password_confirmation">
                                                @error('password_confirmation')
                                                    <small class="text-danger">{{ $message }}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tg-checkbox mb-4">
                                        <div class="custom-control custom-checkbox">
                                            <input name="terms" type="checkbox"
                                                class="custom-control-input @error('terms') is-invalid @enderror"
                                                id="tg-agreetermsandrules">
                                            <label class="custom-control-label" for="tg-agreetermsandrules">I agree
                                                to all <a href="javascript:void(0);">Terms of Use &amp;
                                                    Posting
                                                    Rules</a></label>
                                        </div>
                                    </div>
                                    <button class="btn btn-common btn-block mb-4" type="submit">Mendaftar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
