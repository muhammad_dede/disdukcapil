@extends('layouts.app')

@section('title', 'Reset Password')

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Reset Password</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Reset Password</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="login section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-12 col-xs-12 px-5">
                    <div class="login-form login-area">
                        <h3>
                            Reset Password
                        </h3>
                        <div class="alert alert-warning">
                            <ol>
                                <li>
                                    <small>Password baru Anda harus beda dengan password sebelumnya!</small>
                                </li>
                            </ol>
                        </div>
                        <form action="{{ route('password.update') }}" role="form" class="login-form" method="POST">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="form-group">
                                <div class="input-icon">
                                    <i class="lni-user"></i>
                                    <input type="text" id="email" class="form-control @error('email') is-invalid @enderror"
                                        name="email" placeholder="Email" value="{{ $email ?? old('email') }}">
                                </div>
                                @error('email')
                                    <small class="text-danger">
                                        <strong>{{ $message }}</strong>
                                    </small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="input-icon">
                                    <i class="lni-lock"></i>
                                    <input name="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                                </div>
                                @error('password')
                                    <small class="text-danger">
                                        <strong>{{ $message }}</strong>
                                    </small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="input-icon">
                                    <i class="lni-lock"></i>
                                    <input name="password_confirmation" type="password" class="form-control"
                                        placeholder="Konfirmasi Password">
                                </div>
                            </div>
                            <div class="mt-4">
                                <button type="submit" class="btn btn-common log-btn mr-2 btn-block">Reset Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
