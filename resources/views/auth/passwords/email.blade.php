@extends('layouts.app')

@section('title', 'Lupa Password')

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Lupa Password</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Lupa Password</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="login section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-12 col-xs-12 px-5">
                    <div class="login-form login-area">
                        <h3>
                            Lupa Password
                        </h3>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form action="{{ route('password.email') }}" role="form" class="login-form" method="POST">
                            @csrf
                            <p class="mb-3 text-info">Masukkan email dan kami akan mengirimkan link untuk mengatur
                                ulang password Anda</p>
                            <div class="form-group">
                                <div class="input-icon">
                                    <i class="lni-user"></i>
                                    <input type="text" id="email" class="form-control @error('email') is-invalid @enderror"
                                        name="email" placeholder="Email" value="{{ old('email') }}">
                                </div>
                                @error('email')
                                    <small class="text-danger">
                                        <strong>{{ $message }}</strong>
                                    </small>
                                @enderror
                            </div>
                            <div class="mt-4">
                                <button type="submit" class="btn btn-common log-btn mr-2 btn-block">Kirim Link Ubah
                                    Password</button>
                                @if (Route::has('login'))
                                    <a href="{{ route('login') }}" class="btn btn-light log-btn mr-2 btn-block">Login</a>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
