@extends('layouts.app')

@section('title', 'Login')

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Login</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Login</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="login section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-12 col-xs-12 px-5">
                    <div class="login-form login-area">
                        <h3>
                            Login
                        </h3>
                        <div class="alert alert-warning">
                            <ol>
                                <li>
                                    <small>- Anda harus login terlebih dahulu sebelum melakukan pengajuan layanan!</small>
                                </li>
                                <li>
                                    <small>- Masukkan email dan password yang digunakan pada saat pendaftaran akun
                                        baru.</small>
                                </li>
                            </ol>
                        </div>
                        <form action="{{ route('login') }}" role="form" class="login-form" method="POST">
                            @csrf
                            <div class="form-group">
                                <div class="input-icon">
                                    <i class="lni-user"></i>
                                    <input type="text" id="email" class="form-control @error('email') is-invalid @enderror"
                                        name="email" placeholder="Email" value="{{ old('email') }}">
                                </div>
                                @error('email')
                                    <small class="text-danger">
                                        <strong>{{ $message }}</strong>
                                    </small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="input-icon">
                                    <i class="lni-lock"></i>
                                    <input name="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                                </div>
                                @error('password')
                                    <small class="text-danger">
                                        <strong>{{ $message }}</strong>
                                    </small>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <div class="custom-control custom-checkbox">
                                    <input name="remember" type="checkbox" class="custom-control-input" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="remember">Ingat Saya</label>
                                </div>
                                @if (Route::has('password.request'))
                                    <a class="forgetpassword" href="{{ route('password.request') }}">Lupa Password?</a>
                                @endif
                            </div>
                            <div class="mt-4">
                                <button type="submit" class="btn btn-common log-btn mr-2">Login</button>
                                @if (Route::has('register'))
                                    <a href="{{ route('register') }}" class="text-muted">Belum punya akun? Daftar</a>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
