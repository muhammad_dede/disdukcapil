@extends('layouts.app')

@section('title', 'Tanya Jawab')

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Tanya Jawab</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Tanya Jawab</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="faq section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="head-faq text-center">
                        <h2 class="section-title">Tanya Jawab</h2>
                    </div>

                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        Apa itu Smart Dukcapil?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse show">
                                <div class="panel-body">
                                    <p>Smart Dukcapil adalah layanan pelaporan pencatatan sipil khusus penduduk cilegon yang
                                        dapat diakses dari mana saja.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        Apa sih tujuan Smart Dukcapil?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                        Smart Dukcapil bertujuan untuk dapat memudahkan penduduk secara mandiri mengajukan
                                        permohonan penerbitan dokumen kependudukan dari mana saja tanpa harus datang ke
                                        Dinas Kependudukan dan Pencatatan Sipil.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        Siapa saja sih yang dapat menggunakan Smart Dukcapil?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                        Layanan Online Smart Dukcapil di Khususkan untuk seluruh warga Cilegon.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
