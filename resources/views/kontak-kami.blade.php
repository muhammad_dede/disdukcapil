@extends('layouts.app')

@section('title', 'Kontak Kami')

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Kontak Kami</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Kontak Kami</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="google-map-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <object style="border:0; height: 450px; width: 100%;"
                        data="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126960.71683987386!2d106.02329915192323!3d-6.0600500557508505!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e418f2db5ad3acb%3A0x2612b7844c39d804!2sDinas%20Kependudukan%20dan%20Catatan%20Sipil%20Kota%20Cilegon!5e0!3m2!1sen!2sid!4v1637738338252!5m2!1sen!2sid"></object>
                </div>
            </div>
        </div>
    </section>

    <section id="content" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-xs-12">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif
                    <form action="{{ route('send-message') }}" class="contact-form" method="POST">
                        @csrf
                        <h2 class="contact-title">
                            Kirim Pesan
                        </h2>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                                id="name" name="name" placeholder="Nama" required>
                                            @error('name')
                                                <div class="help-block with-errors text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="email" name="email"
                                                class="form-control @error('email') is-invalid @enderror" id="email"
                                                placeholder="Email" required>
                                            @error('email')
                                                <div class="help-block with-errors text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control @error('subject') is-invalid @enderror"
                                                id="msg_subject" name="subject" placeholder="Subjek" required>
                                            @error('subject')
                                                <div class="help-block with-errors text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea name="message"
                                                class="form-control @error('message') is-invalid @enderror"
                                                placeholder="Pesan Anda" rows="7" required></textarea>
                                            @error('message')
                                                <div class="help-block with-errors text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-common">Kirim Pesan</button>
                                <div id="msgSubmit" class="h3 text-center hidden"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="information mb-4">
                        <h3>Alamat Kami</h3>
                        <div class="contact-datails">
                            <p>Jl. Kp. Jombang Kali No. 46, RT. 4 RW. 1, Kel. Ramanuju Kec. Purwakarta Kota Cilegon, Banten
                                42431.</p>
                        </div>
                    </div>
                    <div class="information">
                        <h3>Informasi Kontak</h3>
                        <div class="contact-datails">
                            <ul class="list-unstyled info">
                                <li><span>Email : </span>
                                    <p>dukcapilcilegon@gmail.com</p>
                                </li>
                                <li><span>Telepon : </span>
                                    <p>(0254) 380577</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
