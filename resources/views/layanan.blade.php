@extends('layouts.app')

@section('title', 'Layanan Kami')

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Layanan Kami</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Layanan</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="pricing-table" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="section-title">Pilih Jenis Layanan</h2>
                </div>
                @forelse (get_layanan() as $layanan)
                    <div class="col-lg-4 col-md-6 col-xs-12 mb-4">
                        <div class="table h-100">
                            <div class="icon">
                                <img src="{{ asset('') }}assets/img/category/img-3.png" alt="">
                            </div>
                            <div class="title">
                                <h3 class="text-info">{{ $layanan->layanan }}</h3>
                            </div>
                            <ul class="description">
                                <li>{{ $layanan->keterangan }}</li>
                            </ul>
                            <a href="{{ route('layanan.detail', $layanan->url) }}" class="btn btn-common ">Lihat
                                Detail</a>
                        </div>
                    </div>
                @empty
                    <strong class="text-center">Tidak ada layanan</strong>
                @endforelse
            </div>
        </div>
    </section>

    <section class="counter-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 work-counter-widget">
                    <div class="counter">
                        <div class="icon"><i class="lni-users"></i></div>
                        <h2 class="counterUp">{{ get_counter()['warga'] }}</h2>
                        <p>Pengguna</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 work-counter-widget">
                    <div class="counter">
                        <div class="icon"><i class="lni-files"></i></div>
                        <h2 class="counterUp">{{ get_counter()['pengajuan'] }}</h2>
                        <p>Total Pengajuan</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 work-counter-widget">
                    <div class="counter">
                        <div class="icon"><i class="lni-timer"></i></div>
                        <h2 class="counterUp">{{ get_counter()['proses'] }}</h2>
                        <p>Dalam Proses</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 work-counter-widget">
                    <div class="counter">
                        <div class="icon"><i class="lni-thumbs-up"></i></div>
                        <h2 class="counterUp">{{ get_counter()['selesai'] }}</h2>
                        <p>Selesai</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
