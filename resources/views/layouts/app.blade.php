<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @hasSection('title')
        <title>@yield('title')&nbsp;-&nbsp;{{ config('app.name') }}</title>
    @else
        <title>{{ config('app.name') }}</title>
    @endif
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('') }}favicon.png">
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}assets/fonts/line-icons.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}assets/css/slicknav.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}assets/css/responsive.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('') }}assets/vendors/sweetalert/sweetalert2.css">
    @stack('my-css')
</head>

<body>
    @include('layouts.app.header')

    @yield('content')

    @include('layouts.app.footer')

    <a href="#" class="back-to-top">
        <i class="lni-chevron-up"></i>
    </a>

    <div id="preloader">
        <div class="loader" id="loader-1"></div>
    </div>

    <script src="{{ asset('') }}assets/js/jquery-min.js"></script>
    <script src="{{ asset('') }}assets/js/popper.min.js"></script>
    <script src="{{ asset('') }}assets/js/bootstrap.min.js"></script>
    <script src="{{ asset('') }}assets/js/jquery.counterup.min.js"></script>
    <script src="{{ asset('') }}assets/js/waypoints.min.js"></script>
    <script src="{{ asset('') }}assets/js/wow.js"></script>
    <script src="{{ asset('') }}assets/js/owl.carousel.min.js"></script>
    <script src="{{ asset('') }}assets/js/jquery.slicknav.js"></script>
    <script src="{{ asset('') }}assets/js/main.js"></script>
    <script src="{{ asset('') }}assets/js/form-validator.min.js"></script>
    <script src="{{ asset('') }}assets/js/contact-form-script.min.js"></script>
    <script src="{{ asset('') }}assets/js/summernote.js"></script>
    <script src="{{ asset('') }}assets/vendors/sweetalert/sweetalert2.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @auth
        <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
        <script src="{{ asset('') }}scripts/pusher.js"></script>
        <script src="{{ asset('') }}scripts/notifikasi/count.js"></script>
    @endauth

    @stack('my-js')
</body>

</html>
