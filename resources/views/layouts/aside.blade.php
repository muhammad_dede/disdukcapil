<aside>
    <div class="sidebar-box">
        <div class="user">
            <figure>
                <img src="{{ asset('') }}profil-user/{{ auth()->user()->profil }}" alt="" class="w-50">
            </figure>
            <div class="usercontent">
                <h3>{{ auth()->user()->nama_lengkap }}</h3>
                <h4>{{ auth()->user()->email }}</h4>
            </div>
        </div>
        <nav class="navdashboard">
            <ul>
                <li>
                    <a class="{{ Request::is('user/pengajuan') || Request::is('user/pengajuan/*') ? 'active' : '' }}"
                        href="{{ route('user.pengajuan.index') }}">
                        <i class="lni-files"></i>
                        <span>Pengajuan Saya</span>
                    </a>
                </li>
                <li>
                    <a class="{{ Request::is('user/notifikasi') || Request::is('user/notifikasi/*') ? 'active' : '' }}"
                        href="{{ route('user.notifikasi.index') }}">
                        <i class="lni-alarm"></i>
                        <span>
                            Notifikasi
                            &nbsp;
                            <sup class="count_notifikasi justify-content-end"></sup>
                        </span>
                    </a>
                </li>
                <li>
                    <a class="{{ Request::is('user/account') ? 'active' : '' }}"
                        href="{{ route('user.account.index') }}">
                        <i class="lni-user"></i>
                        <span>Akun Saya</span>
                    </a>
                </li>
                <li>
                    <a class="{{ Request::is('user/account/change-password') ? 'active' : '' }}"
                        href="{{ route('user.account.change-password') }}">
                        <i class="lni-cog"></i>
                        <span>Ubah Password</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();document.getElementById('form-logout').submit();">
                        <i class="lni-enter"></i>
                        <span>Logout</span>
                    </a>
                    <form id="form-logout" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
    </div>
</aside>
