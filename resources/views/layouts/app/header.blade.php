<header id="header-wrap">
    <nav class="navbar navbar-expand-lg bg-white fixed-top shadow">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar"
                    aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span class="lni-menu"></span>
                    <span class="lni-menu"></span>
                    <span class="lni-menu"></span>
                </button>
                <a href="{{ route('index') }}" class="navbar-brand">
                    <img src="{{ asset('') }}logo.png" alt="logo">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="main-navbar">
                <ul class="navbar-nav mr-auto w-100 justify-content-center">
                    <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('index') }}">
                            Home
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('layanan') || Request::is('layanan/*') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('layanan') }}">
                            Layanan
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('tanya-jawab') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('tanya-jawab') }}">
                            Tanya Jawab
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('kontak-kami') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('kontak-kami') }}">
                            Kontak Kami
                        </a>
                    </li>
                    @guest
                        <li class="nav-item {{ Request::is('login') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('login') }}">
                                Login
                            </a>
                        </li>
                    @endguest
                    @auth
                        <li
                            class="nav-item dropdown {{ Request::is('user/pengajuan') || Request::is('user/pengajuan/*') ? 'active' : '' }}">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                Pengajuan
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item {{ Request::is('user/pengajuan') ? 'active' : '' }}"
                                    href="{{ route('user.pengajuan.index') }}">
                                    Pengajuan Saya
                                </a>
                                <a class="dropdown-item {{ Request::is('user/pengajuan/*/create') ? 'active' : '' }}"
                                    href="{{ route('layanan') }}">
                                    Buat Pengajuan Baru
                                </a>
                            </div>
                        </li>
                    @endauth
                </ul>
                @guest
                    <div class="post-btn">
                        <a class="btn btn-common" href="{{ route('register') }}"><i class="lni-pencil-alt"></i>
                            Pendaftaran Baru</a>
                    </div>
                @endguest
                @auth
                    <div class="post-btn">
                        <a class="d-flex align-items-center" href="{{ route('user.account.index') }}">
                            <span
                                class="text-muted">{{ Str::words(strtoupper(auth()->user()->nama_lengkap), '1', '') }}</span>
                            <img src="{{ asset('') }}profil-user/{{ auth()->user()->profil }}"
                                class="rounded-circle ml-2" height="35" width="35" alt="profil">
                        </a>
                    </div>
                @endauth
            </div>
        </div>

        <ul class="mobile-menu">
            <li class="{{ Request::is('/') ? 'active' : '' }}">
                <a href="{{ route('index') }}">Home</a>
            </li>
            <li class="{{ Request::is('layanan') ? 'active' : '' }}">
                <a href="{{ route('layanan') }}">Jenis
                    Layanan</a>
            </li>
            <li class="{{ Request::is('tanya-jawab') ? 'active' : '' }}">
                <a href="{{ route('tanya-jawab') }}">Tanya Jawab</a>
            </li>
            <li class="{{ Request::is('kontak-kami') ? 'active' : '' }}">
                <a href="{{ route('kontak-kami') }}">Kontak Kami</a>
            </li>
            @guest
                <li class="{{ Request::is('login') ? 'active' : '' }}">
                    <a href="{{ route('login') }}">Login</a>
                </li>
                <li class="{{ Request::is('register') ? 'active' : '' }}">
                    <a href="{{ route('register') }}">Daftar</a>
                </li>
            @endguest
            @auth
                <li class="{{ Request::is('user/pengajuan') || Request::is('user/pengajuan/*') ? 'active' : '' }}">
                    <a href="{{ route('user.pengajuan.index') }}">Pengajuan Saya</a>
                </li>
                <li class="{{ Request::is('user/pengajuan/*/create') ? 'active' : '' }}">
                    <a href="{{ route('layanan') }}">Buat Pengajuan Baru</a>
                </li>
                <li class="{{ Request::is('user/account') ? 'active' : '' }}">
                    <a href="{{ route('user.account.index') }}">
                        {{ Str::words(ucwords(auth()->user()->nama_lengkap), '2', '') }}
                    </a>
                </li>
            @endauth
        </ul>

    </nav>

</header>
