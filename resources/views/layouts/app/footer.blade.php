<footer>
    <section class="footer-Content">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-xs-6 col-mb-12">
                    <div class="widget">
                        <div class="footer-logo">
                            <img src="{{ asset('') }}favicon.png" alt="logo" class="w-25">
                        </div>
                        <div class="textwidget">
                            <p>INOVASI LAYANAN PENGGANTI TATAP MUKA. PEMOHON ATAU WARGA DAPAT
                                MENGURUS DOKUMEN DARI MANAPUN DAN KAPANPUN</p>
                        </div>
                        <ul class="mt-3 footer-social">
                            <li><a class="facebook" href="https://www.facebook.com/disdukcapilkotacilegon"
                                    target="_blank"><i class="lni-facebook-filled"></i></a></li>
                            <li><a class="twitter" href="http://disdukcapil.cilegon.go.id/" target="_blank"><i
                                        class="lni-twitter-filled"></i></a></li>
                            <li><a class="linkedin" href="https://www.instagram.com/disdukcapilkotacilegon/"
                                    target="_blank"><i class="lni-instagram-filled"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-6 col-mb-12">
                    <div class="widget">
                        <h3 class="block-title">Jelajahi Kami</h3>
                        <ul class="menu">
                            <li><a href="{{ route('index') }}">- Home</a></li>
                            <li><a href="{{ route('layanan') }}">- Jenis Layanan</a></li>
                            <li><a href="{{ route('tanya-jawab') }}">- Tanya Jawab</a></li>
                            <li><a href="{{ route('kontak-kami') }}">- Kontak Kami</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-6 col-mb-12">
                    <div class="widget">
                        <h3 class="block-title">Kontak Kami</h3>
                        <ul class="contact-footer">
                            <li>
                                <strong><i class="lni-phone"></i></strong><span>(0254) 380577</span>
                            </li>
                            <li>
                                <strong><i class="lni-envelope"></i></strong><span>
                                    dukcapilcilegon@gmail.com
                                </span>
                            </li>
                            <li>
                                <strong><i class="lni-map-marker"></i></strong><span>Jl. Kp. Jombang Kali
                                    No. 46, RT. 4 RW. 1, Kel. Ramanuju Kec. Purwakarta Kota Cilegon,
                                    Banten 42431</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="copyright">
        <div class="container border-top pt-3 pb-2">
            <div class="row">
                <div class="col-md-12">
                    <div class="site-info text-center">
                        <p><a target="_blank" href="{{ route('index') }}">Copyright © {{ date('Y') }} Pemerintah
                                Kota Cilegon.
                                All Rights Reserved.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
