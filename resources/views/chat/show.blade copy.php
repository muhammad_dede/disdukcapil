@extends('layouts.app')

@section('title', 'Chat')

@push('my-css')
<style>
    /* .scroll {
                                                                                                                                                                                    height: 400px;
                                                                                                                                                                                    overflow-x: hidden;
                                                                                                                                                                                    overflow-y: auto;
                                                                                                                                                                                } */

    ::-webkit-scrollbar {
        width: 5px;
    }

    ::-webkit-scrollbar-track {
        width: 5px;
        background: #f5f5f5;
    }

    ::-webkit-scrollbar-thumb {
        width: 1em;
        background-color: #ddd;
        outline: 1px solid slategrey;
        border-radius: 1rem;
    }

    .text-small {
        font-size: 0.9rem;
    }

    .messages-box,
    .chat-box {
        height: 510px;
        overflow-y: scroll;
    }

    .rounded-lg {
        border-radius: 0.5rem;
    }

    input::placeholder {
        font-size: 0.9rem;
        color: #999;
    }
</style>
@endpush

@section('content')
<div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb-wrapper">
                    <h2 class="product-title">Chat</h2>
                    <ol class="breadcrumb">
                        <li><a href="{{ route('index') }}">Home /</a></li>
                        <li class="current">Chat</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="content" class="section-padding">
    <div class="container">
        <div class="row">
            @if (Session::has('success'))
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ Session::get('success') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            @endif
            <div class="col-sm-12 col-md-4 col-lg-3 page-sidebar">
                @include('layouts.aside')
            </div>
            <div class="col-sm-12 col-md-8 col-lg-9">
                <div class="page-content">
                    <div class="inner-box">
                        <div class="dashboard-box">
                            <h2 class="dashbord-title">Chat Pengajuan {{ $pengajuan->layanan->layanan }}</h2>
                        </div>
                        <div class="dashboard-wrapper">
                            <div class="dashboard-wrapper">
                                <div class="px-4 py-2 chat-box bg-white">
                                    <!-- Sender Message-->
                                    <div class="media w-50 mb-3"><img src="https://bootstrapious.com/i/snippets/sn-chat/avatar.svg" alt="user" width="50" class="rounded-circle">
                                        <div class="media-body ml-3">
                                            <div class="bg-light rounded py-2 px-3 mb-2">
                                                <p class="text-small mb-0 text-muted">Test which is a new approach all
                                                    solutions</p>
                                            </div>
                                            <p class="small text-muted">12:00 PM | Aug 13</p>
                                        </div>
                                    </div>

                                    <!-- Reciever Message-->
                                    <div class="media w-50 ml-auto mb-3">
                                        <div class="media-body">
                                            <div class="bg-primary rounded py-2 px-3 mb-2">
                                                <p class="text-small mb-0 text-white">Test which is a new approach to
                                                    have all solutions</p>
                                            </div>
                                            <p class="small text-muted">12:00 PM | Aug 13</p>
                                        </div>
                                    </div>

                                </div>
                                <form action="{{ route('user.chat.send', $pengajuan->no_pengajuan) }}" class="bg-light" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="input-group">
                                        <input type="text" placeholder="Type a message" aria-describedby="button-addon2" class="form-control rounded-0 border-0 py-3 bg-light" name="message" id="message">
                                        <div class="input-group-append">
                                            <button id="button-addon2" type="submit" class="btn btn-primary">
                                                Kirim
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('my-js')
<script>
    $('.form_chat').submit(function(event) {
        event.preventDefault();
        alert('ok');
    });
</script>
@endpush
