@extends('layouts.app')

@section('title', 'Detail Layanan')

@push('my-css')
    <style>
        .detail-layanan {
            padding: 0px 15px;
        }

        .detail-layanan ol li {
            list-style-type: decimal !important;
        }

    </style>
@endpush

@section('content')
    <div class="page-header" style="background: url({{ asset('') }}assets/img/banner1.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb-wrapper">
                        <h2 class="product-title">Detail Layanan</h2>
                        <ol class="breadcrumb">
                            <li><a href="{{ route('index') }}">Home /</a></li>
                            <li class="current">Detail Layanan</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section id="content" class="section-padding">
        <div class="container">
            <div class="row">
                {{-- Card 1 --}}
                <div class="col-md-12 col-lg-12 col-xs-12">
                    <div class="services-item rounded" style="background-color: #6258C6;">
                        <div class="icon">
                            <i class="lni-bookmark-alt"></i>
                        </div>
                        <div class="services-content">
                            <h3><a href="#" class="text-white">{{ $layanan->layanan }}</a></h3>
                            <p class="text-white">{{ $layanan->keterangan }}</p>
                        </div>
                    </div>
                </div>
                {{-- Card 2 --}}
                <div class="col-md-12 col-lg-12 col-xs-12">
                    <div class="services-item rounded text-dark">
                        <div class="icon border">
                            <i class="lni-empty-file"></i>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="services-content">
                                    <h3 class="border-bottom mb-3">
                                        <a href="#">Informasi & Dokumen Layanan</a>
                                    </h3>
                                    <div class="row">
                                        <div class="col-md-12 text-dark mb-3">
                                            <strong>Persyaratan</strong>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="persyaratan"
                                                    {{ $layanan->layananPersyaratan->count() > 0 ? 'checked' : '' }}
                                                    onclick="return false;">
                                                <label class="form-check-label" for="persyaratan">
                                                    Pemohon harus mengupload dokumen persyaratan sebelum pengajuan dapat
                                                    diproses lebih
                                                    lanjut.
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-dark mb-3">
                                            <strong>Pengambilan Dokumen Kependudukan</strong>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value=""
                                                    id="pengambilan_sendiri"
                                                    {{ $layanan->pengambilan_sendiri ? 'checked' : '' }}
                                                    onclick="return false;">
                                                <label class="form-check-label" for="pengambilan_sendiri">
                                                    Pemohon dapat mengambil dokumen kependudukan sendiri
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="" id="cetak_dokumen"
                                                    {{ $layanan->cetak_dokumen ? 'checked' : '' }}
                                                    onclick="return false;">
                                                <label class="form-check-label" for="cetak_dokumen">
                                                    Pemohon dapat mencetak dokumen kependudukan sendiri
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value=""
                                                    id="pengiriman_ekspedisi"
                                                    {{ $layanan->pengiriman_ekspedisi ? 'checked' : '' }}
                                                    onclick="return false;">
                                                <label class="form-check-label" for="pengiriman_ekspedisi">
                                                    Dokumen Kependudukan dapat dikirimkan kepada pemohon via ekspedisi
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-dark mb-3">
                                            <strong>Dokumen Pendukung</strong>
                                            <div class="detail-layanan">
                                                @if ($layanan->layananDokumen)
                                                    <ol>
                                                        @foreach ($layanan->layananDokumen as $dokumen)
                                                            @if ($dokumen->aktif)
                                                                <li>{{ $dokumen->dokumen }}&nbsp;|&nbsp;<a
                                                                        href="{{ route('user.pengajuan.download-dokumen', $dokumen->file) }}">Download</a>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ol>
                                                @else
                                                    <ul>
                                                        <li><span class="text-warning">Tidak ada dokumen pendukung.</span>
                                                        </li>
                                                    </ul>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="services-content">
                                    <h3 class="border-bottom mb-3">
                                        <a href="#">Mekanisme Pengajuan</a>
                                    </h3>
                                    <p>Mekanisme pengajuan yang harus dilakukan oleh pemohon terkait layanan ini:</p>
                                    <div class="detail-layanan">
                                        <ol>
                                            <li>Pemohon menyiapkan persyaratan yang dibutuhkan</li>
                                            <li>Pemohon melakukan pengajuan Layanan</li>
                                            <li>Pemohon mengisi form pengajuan dengan data isian yang diperlukan</li>
                                            <li>Pemohon mengupload dokumen yang dibutuhkan</li>
                                            <li>Operator memproses pengajuan dari pemohon</li>
                                            <li>Operator menerbitkan dokumen kependudukan</li>
                                            <li>Pemohon mengambil sendiri dokumen kependudukan ke Kantor Dinas</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Card 3 --}}
                <div class="col-md-12 col-lg-12 col-xs-12">
                    <div class="services-item rounded text-dark">
                        <div class="icon">
                            <i class="lni-bookmark-alt"></i>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="services-content">
                                    <h3 class="border-bottom mb-3">
                                        <a href="#">Persyaratan</a>
                                    </h3>
                                    <p>Daftar persyaratan yang harus diupload oleh pemohon sebelum melakukan pengajuan
                                        layanan:</p>
                                    <div class="detail-layanan">
                                        @if ($layanan->layananPersyaratan)
                                            <ol>
                                                @foreach ($layanan->layananPersyaratan as $persyaratan)
                                                    @if ($persyaratan->aktif)
                                                        <li>{{ $persyaratan->persyaratan }}</li>
                                                    @endif
                                                @endforeach
                                            </ol>
                                        @else
                                            <ul>
                                                <li><span class="text-warning">Tanpa persyaratan.</span>
                                                </li>
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="services-content">
                                    <h3 class="border-bottom mb-3">
                                        <a href="#">Isian Pengajuan</a>
                                    </h3>
                                    <p>Daftar isian pada form pengajuan yang harus diisi oleh pemohon:</p>
                                    <div class="detail-layanan">
                                        @if ($layanan->layananIsian)
                                            {!! $layanan->layananIsian->isian !!}
                                        @else
                                            <ul>
                                                <li><span class="text-warning">Tanpa Form Isian.</span>
                                                </li>
                                            </ul>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Card 4 --}}
                <div class="col-md-12 col-lg-12 col-xs-12">
                    <div class="services-item rounded">
                        <div class="icon">
                            <i class="lni-pencil-alt"></i>
                        </div>
                        <div class="services-content">
                            <h3 class="border-bottom mb-3">
                                <a href="#">Pengajuan Baru</a>
                            </h3>
                            <p>Klik pada tombol berikut untuk melakukan pengajuan layanan {{ $layanan->layanan }}</p>
                            <a href="{{ route('user.pengajuan.create', $layanan->url) }}" class="btn btn-common mt-3"><i
                                    class="lni-pencil-alt"></i> Buat Pengajuan Baru</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
